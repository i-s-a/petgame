﻿using System;
using System.Threading.Tasks;
using PetGameClient;
using PetGameClient.Requests;
using PetGameClient.Responses;

namespace PetGameTestClient
{
    class Program
    {

        private static async Task RunUserTests()
        {
            try
            {
                var userClientConnection = new UserClientConnection("http://localhost:62611/api/v1/");

                var userResponse = await userClientConnection.AddUser(new NewUserRequest()
                {
                    Username = "Luke Skywalker"
                });

                System.Console.WriteLine("Created user - {0}, {1}", userResponse.Id, userResponse.Name);

                //var userResponse = await userClientConnection.AddUser(new NewUserRequest()
                //{
                //    Username = ""
                //});

                var updatedUserResponse = await userClientConnection.UpdateUser(userResponse.Id, new UpdateUserRequest()
                {
                    NewName = "Han Solo"
                });

                System.Console.WriteLine("Updated user from {0} to {1}", userResponse.Name, updatedUserResponse.Name);

                //var updatedUserResponse = await userClientConnection.UpdateUser("random", new UpdateUserRequest()
                //{
                //    NewName = "Han Solo"
                //});

                //var updatedUserResponse = await userClientConnection.UpdateUser(userResponse.Id, new UpdateUserRequest()
                //{
                //    NewName = ""
                //});

                var retrievedUserResponse = await userClientConnection.GetUser(updatedUserResponse.Id);

                System.Console.WriteLine("Retrieved user {0}, {1}", retrievedUserResponse.Id, retrievedUserResponse.Name);

                var userResponse2 = await userClientConnection.AddUser(new NewUserRequest()
                {
                    Username = "Joe Bloggs"
                });

                System.Console.WriteLine("Created user - {0}, {1}", userResponse2.Id, userResponse2.Name);

                var userResponseList = await userClientConnection.GetAllUsers();

                foreach (var user in userResponseList)
                {
                    System.Console.WriteLine("Retrieved user - {0}, {1}", user.Id, user.Name);
                }

                System.Console.Write("\n\n");

                var deletedUserResponse = await userClientConnection.DeleteUser(updatedUserResponse.Id);

                System.Console.WriteLine("Deleted user - {0}", deletedUserResponse);

                userResponseList = await userClientConnection.GetAllUsers();

                foreach (var user in userResponseList)
                {
                    System.Console.WriteLine("Retrieved user - {0}, {1}", user.Id, user.Name);
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private static async Task RunUserAndAnimalsTest()
        {
            try
            {
                var userClientConnection = new UserClientConnection("http://localhost:62611/api/v1/");
                //var userClientConnection = new UserClientConnection("http://localhost:62610/api/v1/");

                var animalClientConnection = new AnimalClientConnection("http://localhost:62611/api/v1/");
                //var animalClientConnection = new AnimalClientConnection("http://localhost:62610/api/v1/");

                var userResponse = await userClientConnection.AddUser(new NewUserRequest()
                {
                    Username = "Luke Skywalker"
                });

                var retrievedUserResponse = await userClientConnection.GetUser(userResponse.Id);

                System.Console.WriteLine("User {0}, Number of pets: {1}", retrievedUserResponse.Id, (retrievedUserResponse.AnimalIds == null ? "0" : retrievedUserResponse.AnimalIds.Count.ToString()));

                var animalResponse = await animalClientConnection.AddAnimal(new NewAnimalRequest()
                {
                    TypeOfAnimal = AnimalType.Cat,
                    Name = "Jane the cat"
                });

                System.Console.WriteLine("Created animal - {0}, {1},  {2}", animalResponse.Id, animalResponse.Name, (animalResponse.OwnerId ?? "No Owner"));

                var userWithAnimalResponse = await userClientConnection.AssociateAnimalWithOwner(retrievedUserResponse.Id, animalResponse.Id);

                System.Console.WriteLine("User {0}, Number of pets: {1}", userWithAnimalResponse.Id, (userWithAnimalResponse.AnimalIds == null ? "0" : userWithAnimalResponse.AnimalIds.Count.ToString()));

                animalResponse = await animalClientConnection.GetAnimal(animalResponse.Id);

                System.Console.WriteLine("Retrieved animal - {0}, {1},  {2}", animalResponse.Id, animalResponse.Name, (animalResponse.OwnerId ?? "No Owner"));

                var userWithoutAnimalResponse = await userClientConnection.RemoveAnimalFromOwner(userWithAnimalResponse.Id, animalResponse.Id);

                System.Console.WriteLine("User {0}, Number of pets: {1}", userWithoutAnimalResponse.Id, (userWithoutAnimalResponse.AnimalIds == null ? "0" : userWithoutAnimalResponse.AnimalIds.Count.ToString()));

                animalResponse = await animalClientConnection.GetAnimal(animalResponse.Id);

                System.Console.WriteLine("Retrieved animal - {0}, {1},  {2}", animalResponse.Id, animalResponse.Name, (animalResponse.OwnerId ?? "No Owner"));
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private static async Task RunAnimalTests()
        {
            try
            {
                var animalClientConnection = new AnimalClientConnection("http://localhost:62611/api/v1/");

                var animalResponse = await animalClientConnection.AddAnimal(new NewAnimalRequest()
                {
                    Name = "Jane the cat",
                    TypeOfAnimal = AnimalType.Cat
                });

                System.Console.WriteLine("Created animal - {0}, {1},  {2}", animalResponse.Id, animalResponse.Name, (animalResponse.OwnerId ?? "No Owner"));

                //var animalResponse = await petGameAnimalClient.AddAnimal(new NewAnimalRequest()
                //{
                //    Name = "",
                //    TypeOfAnimal = AnimalType.Cat
                //});

                var updatedAnimalResponse = await animalClientConnection.UpdateAnimal(animalResponse.Id, new UpdateAnimalRequest()
                {
                    NewName = "Joe the cat"
                });

                System.Console.WriteLine("Updated animal from {0} to {1}", animalResponse.Name, updatedAnimalResponse.Name);

                //var updatedAnimalResponse = await petGameAnimalClient.UpdateAnimal("random", new UpdateAnimalRequest()
                //{
                //    NewName = "Joe the cat"
                //});

                //var updatedAnimalResponse = await petGameAnimalClient.UpdateAnimal(animalResponse.Id, new UpdateAnimalRequest()
                //{
                //    NewName = ""
                //});

                var retrievedAnimalResponse = await animalClientConnection.GetAnimal(updatedAnimalResponse.Id);

                System.Console.WriteLine("Retrieved animal {0}, {1}", retrievedAnimalResponse.Id, retrievedAnimalResponse.Name);

                var animalResponse2 = await animalClientConnection.AddAnimal(new NewAnimalRequest()
                {
                    TypeOfAnimal = AnimalType.Dog,
                    Name = "Sooty the dog"
                });

                System.Console.WriteLine("Created animal - {0}, {1},  {2}", animalResponse2.Id, animalResponse2.Name, (animalResponse2.OwnerId ?? "No Owner"));

                var animalResponseList = await animalClientConnection.GetAllAnimals();

                foreach (var animal in animalResponseList)
                {
                    System.Console.WriteLine("Retrieved animal - {0}, {1}", animal.Id, animal.Name);
                }

                System.Console.Write("\n\n");

                var deletedanimalResponse = await animalClientConnection.DeleteAnimal(updatedAnimalResponse.Id);

                System.Console.WriteLine("Deleted animal - {0}", deletedanimalResponse);

                animalResponseList = await animalClientConnection.GetAllAnimals();

                foreach (var animal in animalResponseList)
                {
                    System.Console.WriteLine("Retrieved animal - {0}, {1}", animal.Id, animal.Name);
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private static async Task RunAnimalInteractionTests()
        {
            try
            {
                var userClientConnection = new UserClientConnection("http://localhost:62611/api/v1/");
                //var userClientConnection = new UserClientConnection("http://localhost:62610/api/v1/");

                var animalClientConnection = new AnimalClientConnection("http://localhost:62611/api/v1/");
                //var animalClientConnection = new AnimalClientConnection("http://localhost:62610/api/v1/");


                var userResponse = await userClientConnection.AddUser(new NewUserRequest()
                {
                    Username = "Luke Skywalker"
                });

                var retrievedUserResponse = await userClientConnection.GetUser(userResponse.Id);

                System.Console.WriteLine("User {0}, Number of pets: {1}", retrievedUserResponse.Id, (retrievedUserResponse.AnimalIds == null ? "0" : retrievedUserResponse.AnimalIds.Count.ToString()));

                var animalResponse = await animalClientConnection.AddAnimal(new NewAnimalRequest()
                {
                    TypeOfAnimal = AnimalType.Cat,
                    Name = "Jane the cat"
                });

                System.Console.WriteLine("Created animal - {0}, {1},  {2}", animalResponse.Id, animalResponse.Name, (animalResponse.OwnerId ?? "No Owner"));

                var userWithAnimalResponse = await userClientConnection.AssociateAnimalWithOwner(retrievedUserResponse.Id, animalResponse.Id);

                System.Console.WriteLine("User {0}, Number of pets: {1}", userWithAnimalResponse.Id, (userWithAnimalResponse.AnimalIds == null ? "0" : userWithAnimalResponse.AnimalIds.Count.ToString()));

                animalResponse = await animalClientConnection.GetAnimal(animalResponse.Id);

                System.Console.WriteLine("Retrieved animal - {0}, {1},  {2}", animalResponse.Id, animalResponse.Name, (animalResponse.OwnerId ?? "No Owner"));
                System.Console.WriteLine("Happiness level: {0}", animalResponse.HappinessLevel);
                System.Console.WriteLine("Hunger level: {0}", animalResponse.HungerLevel);
                System.Console.WriteLine("Cleanliness level: {0}", animalResponse.CleanlinessLevel);

                animalResponse = await animalClientConnection.StrokeAnimal(new StrokeAnimalRequest()
                {
                    UserId = retrievedUserResponse.Id,
                    AnimalId = animalResponse.Id
                });

                animalResponse = await animalClientConnection.FeedAnimal(new FeedAnimalRequest()
                {
                    UserId = retrievedUserResponse.Id,
                    AnimalId = animalResponse.Id
                });

                animalResponse = await animalClientConnection.WashAnimal(new WashAnimalRequest()
                {
                    UserId = retrievedUserResponse.Id,
                    AnimalId = animalResponse.Id
                });

                System.Console.WriteLine("Happiness level: {0}", animalResponse.HappinessLevel);
                System.Console.WriteLine("Hunger level: {0}", animalResponse.HungerLevel);
                System.Console.WriteLine("Cleanliness level: {0}", animalResponse.CleanlinessLevel);

            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private static async Task RunCreditTests()
        {
            try
            {
                var creditClientConnection = new CreditClientConnection("http://localhost:62611/api/v1/");
                //var creditClientConnection = new CreditClientConnection("http://localhost:62610/api/v1/");

                var creditResponse = await creditClientConnection.CreateOrUpdateUserCredit(Guid.NewGuid().ToString(),
                    new UpdateUserCreditRequest()
                    {
                        Amount = 100
                    });

                Console.WriteLine("User {0} has {1} credits", creditResponse.Id, creditResponse.Amount);

                var updatedCreditResponse = await creditClientConnection.CreateOrUpdateUserCredit(
                    creditResponse.Id, new UpdateUserCreditRequest()
                    {
                        Amount = 10
                    });

                Console.WriteLine("User {0} has {1} credits", updatedCreditResponse.Id, updatedCreditResponse.Amount);

                updatedCreditResponse = await creditClientConnection.CreateOrUpdateUserCredit(
                    creditResponse.Id, new UpdateUserCreditRequest()
                    {
                        Amount = -20
                    });

                Console.WriteLine("User {0} has {1} credits", updatedCreditResponse.Id, updatedCreditResponse.Amount);

                var retrievedCreditResponse = await creditClientConnection.GetUserCredit(creditResponse.Id);
                Console.WriteLine("User {0} has {1} credits", retrievedCreditResponse.Id, retrievedCreditResponse.Amount);

                try
                {
                    var _ = await creditClientConnection.DeleteUserCredit(creditResponse.Id);
                }
                catch(PetGameClientException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                var deletedCreditResponse = await creditClientConnection.DeleteUserCredit(creditResponse.Id, true);
                Console.WriteLine("User {0} deleted", deletedCreditResponse);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        static async Task Main(string[] args)
        {
            await RunUserTests();
            System.Console.Write("------------{0}{1}", Environment.NewLine, Environment.NewLine);
            System.Console.ReadKey();
            await RunAnimalTests();
            System.Console.Write("------------{0}{1}", Environment.NewLine, Environment.NewLine);
            System.Console.ReadKey();
            await RunUserAndAnimalsTest();
            System.Console.Write("------------{0}{1}", Environment.NewLine, Environment.NewLine);
            System.Console.ReadKey();
            await RunAnimalInteractionTests();
            System.Console.Write("------------{0}{1}", Environment.NewLine, Environment.NewLine);
            System.Console.ReadKey();
            await RunCreditTests();
            System.Console.Write("------------{0}{1}", Environment.NewLine, Environment.NewLine);
            System.Console.ReadKey();
        }
    }
}
