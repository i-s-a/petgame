﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PetGame;
using PetGame.Requests;
using PetGame.Models;
using PetGame.Services;
using PetGame.Repositories;
using System.Threading.Tasks;
using System.Linq;

namespace PetGameTest
{
    [TestClass]
    public class UserServiceTest
    {
        private UserService mUserService = null;
        private AnimalService mAnimalService = null;
        private ConfigService mConfigService = null;
        private static bool mSetup = false;
        private DateTime mNow;

        [TestInitialize]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                .UseInMemoryDatabase(databaseName: "TestRepository_UserService")
                .Options;

            mNow = DateTime.Now;
            IAnimalStateChangeStrategy animalStateChangeStrategy = new AnimalStateStrategyPerMinute_Test(mNow);

            ApiContext apiContext = new ApiContext(options);

            IConfigRepository configRepository = new InMemoryConfigRepository(apiContext);
            IUserRepository userRepository = new InMemoryUserRepository(apiContext);
            IAnimalRepository animalRepository = new InMemoryAnimalRepository(apiContext);

            mConfigService = new ConfigService(configRepository);
            CreditService creditService = new CreditService(new InMemoryCreditRepository(apiContext));
            mAnimalService = new AnimalService(animalRepository, animalStateChangeStrategy, creditService, mConfigService);
            mUserService = new UserService(userRepository, mAnimalService, creditService);
            
            if (!mSetup)
            {
                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Cat,
                    HappinessChangePerEvent = 2,
                    HappinessChangePerStroke = 6,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 4,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 5,
                    CleanlinessChangePerEvent = 4,
                    CleanlinessChangePerWash = 8,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 8,
                    Description = "cat"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Dog,
                    HappinessChangePerEvent = 4,
                    HappinessChangePerStroke = 8,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 8,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 10,
                    CleanlinessChangePerEvent = 2,
                    CleanlinessChangePerWash = 6,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 12,
                    Description = "dog"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Rabbit,
                    HappinessChangePerEvent = 10,
                    HappinessChangePerStroke = 20,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 4,
                    HungerChangePerFeed = 8,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 3,
                    CleanlinessChangePerEvent = 1,
                    CleanlinessChangePerWash = 4,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 5,
                    Description = "rabbit"
                });

                mSetup = true;
            }
        }

        [TestMethod]
        public async Task AddUser_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var retrievedUser = await mUserService.GetUser(user.Id);
            Assert.IsNotNull(retrievedUser);
            Assert.IsTrue(retrievedUser.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);
        }

        [TestMethod]
        public async Task AddUser_NoName_Test()
        {
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mUserService.AddUser(new NewUserRequest() { Username = "" }));
        }

        [TestMethod]
        public async Task AddUser_And_1_Animal_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);
            /////
            //Maybe also check the happiness, hunger and cleanliness level???
            /////

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);

            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));

            var retrievedUser = await mUserService.GetUser(user.Id);

            Assert.IsNotNull(retrievedUser);
            Assert.IsTrue(retrievedUser.Id == user.Id);
            Assert.IsTrue(retrievedUser.Name == "Han Solo");
            Assert.IsNotNull(retrievedUser.AnimalIds);
            Assert.IsTrue(retrievedUser.AnimalIds.Count == 1);
            Assert.IsTrue(retrievedUser.AnimalIds[0] == animal.Id);
        }

        [TestMethod]
        public async Task AddUser_And_Multiple_Animals_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            var animal2 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal2);
            Assert.IsTrue(animal2.Name == "Jane the cat");
            Assert.IsTrue(animal2.TypeOfAnimal == AnimalType.Cat);

            var animal3 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Bobby the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal3);
            Assert.IsTrue(animal3.Name == "Bobby the cat");
            Assert.IsTrue(animal3.TypeOfAnimal == AnimalType.Cat);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal2.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal2.Id));
            Assert.IsTrue(user.AnimalIds.Count == 2);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal3.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal3.Id));
            Assert.IsTrue(user.AnimalIds.Count == 3);
        }

        [TestMethod]
        public async Task DeleteUser_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            var retreivedAnimal = await mAnimalService.GetAnimal(animal1.Id);
            Assert.IsNotNull(retreivedAnimal);
            Assert.IsTrue(retreivedAnimal.OwnerId == user.Id);

            var deletedUser = await mUserService.DeleteUser(user.Id);
            Assert.IsTrue(deletedUser == user.Id);

            retreivedAnimal = await mAnimalService.GetAnimal(animal1.Id);
            Assert.IsNotNull(retreivedAnimal);
            Assert.IsNull(retreivedAnimal.OwnerId);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.GetUser(user.Id));
        }

        [TestMethod]
        public async Task DeleteUser_UserNotExist_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            var retreivedAnimal = await mAnimalService.GetAnimal(animal1.Id);
            Assert.IsNotNull(retreivedAnimal);
            Assert.IsTrue(retreivedAnimal.OwnerId == user.Id);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.GetUser("random"));
        }

        [TestMethod]
        public async Task UpdateUser_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var updatedUser = await mUserService.UpdateUser(user.Id, new UpdateUserRequest() { NewName = "Luke Skywalker" });
            Assert.IsTrue(updatedUser.Id == user.Id);
            Assert.IsTrue(updatedUser.Name == "Luke Skywalker");
        }

        [TestMethod]
        public async Task UpdateUser_NoName_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var updatedUser = mUserService.UpdateUser(user.Id, new UpdateUserRequest() { NewName = "" });
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => updatedUser);
        }

        [TestMethod]
        public async Task UpdateUser_UserNotExist_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var updatedUser = mUserService.UpdateUser("random", new UpdateUserRequest() { NewName = "Luke Skywalker" });
            await Assert.ThrowsExceptionAsync<NotFoundException>(() => updatedUser);
        }

        [TestMethod]
        public async Task GetUserAnimal_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            var retreivedAnimal = await mAnimalService.GetAnimal(animal1.Id);
            Assert.IsNotNull(retreivedAnimal);
            Assert.IsTrue(retreivedAnimal.OwnerId == user.Id);
            Assert.IsTrue(retreivedAnimal.Name == "Joe the dog");
            Assert.IsTrue(retreivedAnimal.TypeOfAnimal == AnimalType.Dog);
        }

        [TestMethod]
        public async Task GetUserAnimal_UserNotExist_Test()
        {
            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.GetAnimal("random", "random"));
        }

        [TestMethod]
        public async Task GetUserAnimal_AnimalNotExist_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            var animal2 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal2);
            Assert.IsTrue(animal2.Name == "Jane the cat");
            Assert.IsTrue(animal2.TypeOfAnimal == AnimalType.Cat);

            var animal3 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Bobby the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal3);
            Assert.IsTrue(animal3.Name == "Bobby the cat");
            Assert.IsTrue(animal3.TypeOfAnimal == AnimalType.Cat);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal2.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal2.Id));
            Assert.IsTrue(user.AnimalIds.Count == 2);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal3.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal3.Id));
            Assert.IsTrue(user.AnimalIds.Count == 3);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mUserService.GetAnimal(user.Id, "random"));
        }

        [TestMethod]
        public async Task GetUserAnimal_AnimalsEmpty_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mUserService.GetAnimal(user.Id, "random"));
        }

        [TestMethod]
        public async Task GetUserAnimals_Test()
        {
            var user1 = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user1);
            Assert.IsTrue(user1.Name == "Luke Skywalker");
            Assert.IsTrue(user1.AnimalIds.Count == 0);

            var user2 = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user2);
            Assert.IsTrue(user2.Name == "Han Solo");
            Assert.IsTrue(user2.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            var animal2 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal2);
            Assert.IsTrue(animal2.Name == "Jane the cat");
            Assert.IsTrue(animal2.TypeOfAnimal == AnimalType.Cat);

            var animal3 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Bobby the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal3);
            Assert.IsTrue(animal3.Name == "Bobby the cat");
            Assert.IsTrue(animal3.TypeOfAnimal == AnimalType.Cat);

            user2 = await mUserService.AssociateAnimalWithOwner(user2.Id, animal1.Id);
            Assert.IsTrue(user2.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user2.AnimalIds.Count == 1);

            user2 = await mUserService.AssociateAnimalWithOwner(user2.Id, animal2.Id);
            Assert.IsTrue(user2.AnimalIds.Exists(x => x == animal2.Id));
            Assert.IsTrue(user2.AnimalIds.Count == 2);

            user2 = await mUserService.AssociateAnimalWithOwner(user2.Id, animal3.Id);
            Assert.IsTrue(user2.AnimalIds.Exists(x => x == animal3.Id));
            Assert.IsTrue(user2.AnimalIds.Count == 3);

            var retreivedAnimals1 = await mUserService.GetAnimals(user1.Id);
            var animals1 = retreivedAnimals1.ToList();
            Assert.IsTrue(animals1.Count == 0);

            var retreivedAnimals2 = await mUserService.GetAnimals(user2.Id);
            var animals2 = retreivedAnimals2.ToList();
            Assert.IsTrue(animals2.Count == 3);
            Assert.IsTrue(animals2.Exists(x => x.Id == animal1.Id));
            Assert.IsTrue(animals2.Exists(x => x.Id == animal2.Id));
            Assert.IsTrue(animals2.Exists(x => x.Id == animal3.Id));
        }

        [TestMethod]
        public async Task GetUserAnimals_ByType_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            var animal2 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal2);
            Assert.IsTrue(animal2.Name == "Jane the cat");
            Assert.IsTrue(animal2.TypeOfAnimal == AnimalType.Cat);

            var animal3 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Bobby the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal3);
            Assert.IsTrue(animal3.Name == "Bobby the cat");
            Assert.IsTrue(animal3.TypeOfAnimal == AnimalType.Cat);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal2.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal2.Id));
            Assert.IsTrue(user.AnimalIds.Count == 2);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal3.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal3.Id));
            Assert.IsTrue(user.AnimalIds.Count == 3);

            var retreivedAnimals = await mUserService.GetAnimals(user.Id, AnimalType.Cat);
            var animals = retreivedAnimals.ToList();
            Assert.IsTrue(animals.Count == 2);
            Assert.IsFalse(animals.Exists(x => x.Id == animal1.Id));
            Assert.IsTrue(animals.Exists(x => x.Id == animal2.Id));
            Assert.IsTrue(animals.Exists(x => x.Id == animal3.Id));
        }

        [TestMethod]
        public async Task GetUserAnimals_ByType_NoAnimals_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var retreivedAnimals = await mUserService.GetAnimals(user.Id, AnimalType.Cat);
            var animals = retreivedAnimals.ToList();
            Assert.IsTrue(animals.Count == 0);
        }

        [TestMethod]
        public async Task GetUserAnimals_ByType_UserNotExists_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.GetAnimals("random", AnimalType.Cat));
        }

        [TestMethod]
        public async Task AssociateUserWithAnimal_AnimalNotExist_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            var animal2 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal2);
            Assert.IsTrue(animal2.Name == "Jane the cat");
            Assert.IsTrue(animal2.TypeOfAnimal == AnimalType.Cat);

            var animal3 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Bobby the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal3);
            Assert.IsTrue(animal3.Name == "Bobby the cat");
            Assert.IsTrue(animal3.TypeOfAnimal == AnimalType.Cat);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.AssociateAnimalWithOwner(user.Id, "random"));
        }

        [TestMethod]
        public async Task AssociateUserWithAnimal_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);
            Assert.IsNull(animal.OwnerId);

            var userWithAnimal = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsNotNull(userWithAnimal);
            Assert.IsTrue(userWithAnimal.Id == user.Id);
            Assert.IsTrue(userWithAnimal.Name == user.Name);
            Assert.IsTrue(userWithAnimal.AnimalIds.Count == 1);
            Assert.IsTrue(userWithAnimal.AnimalIds[0] == animal.Id);

            var animalWithOwner = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsNotNull(animalWithOwner);
            Assert.IsTrue(animalWithOwner.Id == animal.Id);
            Assert.IsTrue(animalWithOwner.Name == animal.Name);
            Assert.IsNotNull(animalWithOwner.OwnerId);
            Assert.IsTrue(animalWithOwner.OwnerId == userWithAnimal.Id);
        }

        [TestMethod]
        public async Task AssociateUserWithAnimal_NoAnimals_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            var userWithAnimal = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
        }

        [TestMethod]
        public async Task RemoveAnimalFromUser_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);
            Assert.IsNull(animal.OwnerId);

            var userWithAnimal = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsNotNull(userWithAnimal);
            Assert.IsTrue(userWithAnimal.Id == user.Id);
            Assert.IsTrue(userWithAnimal.Name == user.Name);
            Assert.IsTrue(userWithAnimal.AnimalIds.Count == 1);
            Assert.IsTrue(userWithAnimal.AnimalIds[0] == animal.Id);

            var animalWithOwner = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsNotNull(animalWithOwner);
            Assert.IsTrue(animalWithOwner.Id == animal.Id);
            Assert.IsTrue(animalWithOwner.Name == animal.Name);
            Assert.IsNotNull(animalWithOwner.OwnerId);
            Assert.IsTrue(animalWithOwner.OwnerId == userWithAnimal.Id);

            var userWithoutAnimal = await mUserService.RemoveAnimalFromOwner(userWithAnimal.Id, animalWithOwner.Id);
            Assert.IsNotNull(userWithoutAnimal);
            Assert.IsTrue(userWithoutAnimal.Id == user.Id);
            Assert.IsTrue(userWithoutAnimal.Name == user.Name);
            Assert.IsTrue(userWithoutAnimal.AnimalIds.Count == 0);

            var animalWithoutOwner = await mAnimalService.GetAnimal(animalWithOwner.Id);
            Assert.IsNotNull(animalWithoutOwner);
            Assert.IsTrue(animalWithoutOwner.Id == animal.Id);
            Assert.IsTrue(animalWithoutOwner.Name == animal.Name);
            Assert.IsNull(animalWithoutOwner.OwnerId);
        }

        [TestMethod]
        public async Task RemoveAnimalFromUser_NoAnimals_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.RemoveAnimalFromOwner(user.Id, animal.Id));
        }

        [TestMethod]
        public async Task RemoveAnimalFromUser_AnimalNotExist_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);
            Assert.IsNull(animal.OwnerId);

            var userWithAnimal = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsNotNull(userWithAnimal);
            Assert.IsTrue(userWithAnimal.Id == user.Id);
            Assert.IsTrue(userWithAnimal.Name == user.Name);
            Assert.IsTrue(userWithAnimal.AnimalIds.Count == 1);
            Assert.IsTrue(userWithAnimal.AnimalIds[0] == animal.Id);

            var animalWithOwner = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsNotNull(animalWithOwner);
            Assert.IsTrue(animalWithOwner.Id == animal.Id);
            Assert.IsTrue(animalWithOwner.Name == animal.Name);
            Assert.IsNotNull(animalWithOwner.OwnerId);
            Assert.IsTrue(animalWithOwner.OwnerId == userWithAnimal.Id);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.RemoveAnimalFromOwner(user.Id, "random"));
        }

        [TestMethod]
        public async Task RemoveAnimalFromUser_UserNotExist_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);
            Assert.IsNull(animal.OwnerId);

            var userWithAnimal = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsNotNull(userWithAnimal);
            Assert.IsTrue(userWithAnimal.Id == user.Id);
            Assert.IsTrue(userWithAnimal.Name == user.Name);
            Assert.IsTrue(userWithAnimal.AnimalIds.Count == 1);
            Assert.IsTrue(userWithAnimal.AnimalIds[0] == animal.Id);

            var animalWithOwner = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsNotNull(animalWithOwner);
            Assert.IsTrue(animalWithOwner.Id == animal.Id);
            Assert.IsTrue(animalWithOwner.Name == animal.Name);
            Assert.IsNotNull(animalWithOwner.OwnerId);
            Assert.IsTrue(animalWithOwner.OwnerId == userWithAnimal.Id);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.RemoveAnimalFromOwner("random", animalWithOwner.Id));
        }
    }
}
