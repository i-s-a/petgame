﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PetGame;
using PetGame.Requests;
using PetGame.Models;
using PetGame.Services;
using PetGame.Repositories;
using System.Threading.Tasks;
using System.Linq;

namespace PetGameTest
{
    [TestClass]
    public class User_And_Credit_ServiceTest
    {
        private UserService mUserService = null;
        private AnimalService mAnimalService = null;
        private CreditService mCreditService = null;
        private ConfigService mConfigService = null;
        private static bool mSetup = false;
        private DateTime mNow;

        [TestInitialize]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                .UseInMemoryDatabase(databaseName: "TestRepository_User_And_Credit_Service")
                .Options;

            mNow = DateTime.Now;
            IAnimalStateChangeStrategy animalStateChangeStrategy = new AnimalStateStrategyPerMinute_Test(mNow);

            ApiContext apiContext = new ApiContext(options);

            IConfigRepository configRepository = new InMemoryConfigRepository(apiContext);
            IUserRepository userRepository = new InMemoryUserRepository(apiContext);
            IAnimalRepository animalRepository = new InMemoryAnimalRepository(apiContext);

            mConfigService = new ConfigService(configRepository);
            mCreditService = new CreditService(new InMemoryCreditRepository(apiContext));
            mAnimalService = new AnimalService(animalRepository, animalStateChangeStrategy, mCreditService, mConfigService);
            mUserService = new UserService(userRepository, mAnimalService, mCreditService);

            if (!mSetup)
            {
                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Cat,
                    HappinessChangePerEvent = 2,
                    HappinessChangePerStroke = 6,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 4,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 5,
                    CleanlinessChangePerEvent = 4,
                    CleanlinessChangePerWash = 8,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 8,
                    Description = "cat"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Dog,
                    HappinessChangePerEvent = 4,
                    HappinessChangePerStroke = 8,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 8,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 10,
                    CleanlinessChangePerEvent = 2,
                    CleanlinessChangePerWash = 6,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 12,
                    Description = "dog"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Rabbit,
                    HappinessChangePerEvent = 10,
                    HappinessChangePerStroke = 20,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 4,
                    HungerChangePerFeed = 8,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 3,
                    CleanlinessChangePerEvent = 1,
                    CleanlinessChangePerWash = 4,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 5,
                    Description = "rabbit"
                });

                mSetup = true;
            }
        }

        [TestMethod]
        public async Task AddUser_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var retrievedUser = await mUserService.GetUser(user.Id);
            Assert.IsNotNull(retrievedUser);
            Assert.IsTrue(retrievedUser.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);
        }

        [TestMethod]
        public async Task DeleteUser_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            var retreivedAnimal = await mAnimalService.GetAnimal(animal1.Id);
            Assert.IsNotNull(retreivedAnimal);
            Assert.IsTrue(retreivedAnimal.OwnerId == user.Id);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

            var deletedUser = await mUserService.DeleteUser(user.Id);
            Assert.IsTrue(deletedUser == user.Id);

            retreivedAnimal = await mAnimalService.GetAnimal(animal1.Id);
            Assert.IsNotNull(retreivedAnimal);
            Assert.IsNull(retreivedAnimal.OwnerId);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mUserService.GetUser(user.Id));
            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mCreditService.GetUserCredit(user.Id));
        }

        [TestMethod]
        public async Task DeleteUser_With_Credit_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Han Solo" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Han Solo");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal1.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal1.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            var retreivedAnimal = await mAnimalService.GetAnimal(animal1.Id);
            Assert.IsNotNull(retreivedAnimal);
            Assert.IsTrue(retreivedAnimal.OwnerId == user.Id);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

            var updatedCredit = await mCreditService.UpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 10
            });

            Assert.IsNotNull(updatedCredit);
            Assert.AreEqual(updatedCredit.Amount, 10);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mUserService.DeleteUser(user.Id));
        }
    }
}
