﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PetGame;
using PetGame.Requests;
using PetGame.Models;
using PetGame.Services;
using PetGame.Repositories;
using System.Threading.Tasks;
using System.Linq;

namespace PetGameTest
{
    [TestClass]
    public class CreditServiceTest
    {
        private CreditService mCreditService = null;

        [TestInitialize]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                .UseInMemoryDatabase(databaseName: "TestRepository_CreditService")
                .Options;

            ApiContext apiContext = new ApiContext(options);

            ICreditRepository creditRepository = new InMemoryCreditRepository(apiContext);
            mCreditService = new CreditService(creditRepository);
        }

        [TestMethod]
        public async Task AddUserCredit()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 0
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 0);
        }

        [TestMethod]
        public async Task UpdateUserCredit_DebitAccount()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 0
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 0);

            var updatedCredit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 10,
            });

            Assert.IsNotNull(updatedCredit);
            Assert.IsTrue(updatedCredit.Id == userId);
            Assert.IsTrue(updatedCredit.Amount == 10);
        }

        [TestMethod]
        public async Task UpdateUserCredit_CreditAccount()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 10
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 10);

            var updatedCredit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = -5,
            });

            Assert.IsNotNull(updatedCredit);
            Assert.IsTrue(updatedCredit.Id == userId);
            Assert.IsTrue(updatedCredit.Amount == 5);
        }

        [TestMethod]
        public async Task UpdateUserCredit_EmptyIt()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 10
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 10);

            var updatedCredit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = -10,
            });

            Assert.IsNotNull(updatedCredit);
            Assert.IsTrue(updatedCredit.Id == userId);
            Assert.IsTrue(updatedCredit.Amount == 0);
        }

        [TestMethod]
        public async Task UpdateUserCredit_NotEnoughCredits()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 10
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 10);

            await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = -11,
            }));
        }

        [TestMethod]
        public async Task GetUserCredit()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 0
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 0);

            var retrievedUserCredit = await mCreditService.GetUserCredit(userId);
            Assert.IsNotNull(retrievedUserCredit);
            Assert.IsTrue(retrievedUserCredit.Id == userId);
            Assert.IsTrue(retrievedUserCredit.Amount == 0);
        }

        [TestMethod]
        public async Task GetUserCredit_UserNotExist()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 0
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 0);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mCreditService.GetUserCredit("random"));
        }

        [TestMethod]
        public async Task DeleteUserCredit_EmptyAccount()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 0
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 0);

            var deletedUserId = await mCreditService.DeleteUserCredit(userId, false);
            Assert.IsTrue(deletedUserId == userId);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mCreditService.GetUserCredit(deletedUserId));
        }

        [TestMethod]
        public async Task DeleteUserCredit_NotEmptyAccount_NotForced()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 10
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 10);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mCreditService.DeleteUserCredit(userId, false));
        }

        [TestMethod]
        public async Task DeleteUserCredit_NotEmptyAccount_Forced()
        {
            var userId = Guid.NewGuid().ToString();

            var credit = await mCreditService.CreateOrUpdateUserCredit(userId, new UpdateUserCreditRequest()
            {
                Amount = 10
            });

            Assert.IsNotNull(credit);
            Assert.IsTrue(credit.Id == userId);
            Assert.IsTrue(credit.Amount == 10);

            var deletedUserId = await mCreditService.DeleteUserCredit(userId, true);
            Assert.IsTrue(deletedUserId == userId);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mCreditService.GetUserCredit(deletedUserId));
        }
    }
}
