﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PetGame;
using PetGame.Requests;
using PetGame.Models;
using PetGame.Services;
using PetGame.Repositories;
using System.Threading.Tasks;
using System.Linq;

namespace PetGameTest
{
    [TestClass]
    public class AnimalServiceTest
    {
        private UserService mUserService = null;
        private AnimalService mAnimalService = null;
        private ConfigService mConfigService = null;
        private static bool mSetup = false;
        private DateTime mNow;

        [TestInitialize]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                .UseInMemoryDatabase(databaseName: "TestRepository_AnimalService")
                .Options;

            mNow = DateTime.Now;
            IAnimalStateChangeStrategy animalStateChangeStrategy = new AnimalStateStrategyPerSecond_Test(mNow);

            ApiContext apiContext = new ApiContext(options);

            IConfigRepository configRepository = new InMemoryConfigRepository(apiContext);
            IUserRepository userRepository = new InMemoryUserRepository(apiContext);
            IAnimalRepository animalRepository = new InMemoryAnimalRepository(apiContext);

            mConfigService = new ConfigService(configRepository);
            CreditService creditService = new CreditService(new InMemoryCreditRepository(apiContext));
            mAnimalService = new AnimalService(animalRepository, animalStateChangeStrategy, creditService, mConfigService);
            mUserService = new UserService(userRepository, mAnimalService, creditService);

            if (!mSetup)
            {
                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Cat,
                    HappinessChangePerEvent = 2,
                    HappinessChangePerStroke = 6,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 4,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 5,
                    CleanlinessChangePerEvent = 4,
                    CleanlinessChangePerWash = 8,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 8,
                    Description = "cat"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Dog,
                    HappinessChangePerEvent = 4,
                    HappinessChangePerStroke = 8,
                    HappinessMaxValue = 102,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 7,
                    HungerMaxValue = 102,
                    HungerCreditsRequiredForFeed = 10,
                    CleanlinessChangePerEvent = 3,
                    CleanlinessChangePerWash = 11,
                    CleanlinessMaxValue = 102,
                    CleanlinessCreditsRequiredForWash = 12,
                    Description = "dog"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Rabbit,
                    HappinessChangePerEvent = 10,
                    HappinessChangePerStroke = 20,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 0,  //Stroking is free
                    HungerChangePerEvent = 4,
                    HungerChangePerFeed = 8,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 3,
                    CleanlinessChangePerEvent = 1,
                    CleanlinessChangePerWash = 4,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 5,
                    Description = "rabbit"
                });

                mSetup = true;
            }
        }

        [TestMethod]
        public async Task AddAnimal_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);
        }

        [TestMethod]
        public async Task AddAnimal_NoConfigTest()
        {
            var animal = mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the hamster", TypeOfAnimal = AnimalType.Hamster });
            await Assert.ThrowsExceptionAsync<Exception>(() => animal);
        }

        [TestMethod]
        public async Task DeleteAnimal_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            var deletedId = await mAnimalService.DeleteAnimal(animal.Id);
            Assert.IsTrue(deletedId == animal.Id);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mAnimalService.GetAnimal(animal.Id));
        }

        [TestMethod]
        public async Task DeleteAnimal_AnimalNotExist_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mAnimalService.DeleteAnimal("random"));
        }

        [TestMethod]
        public async Task DeleteAnimal_AnimalHasOwner()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            var retrievedAnimal = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsNotNull(retrievedAnimal);
            Assert.IsTrue(retrievedAnimal.Id == animal.Id);
            Assert.IsTrue(retrievedAnimal.Name == "Jane the cat");
            Assert.IsTrue(retrievedAnimal.OwnerId == user.Id);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.DeleteAnimal(animal.Id));
        }

        [TestMethod]
        public async Task DeleteAnimal_AnimalDeletedAfterOwnerDeleted()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            var retrievedAnimal = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsNotNull(retrievedAnimal);
            Assert.IsTrue(retrievedAnimal.Id == animal.Id);
            Assert.IsTrue(retrievedAnimal.Name == "Jane the cat");
            Assert.IsTrue(retrievedAnimal.OwnerId == user.Id);

            var deletedUserId = await mUserService.DeleteUser(user.Id);
            Assert.IsTrue(deletedUserId == user.Id);

            var deletedAnimalId = await mAnimalService.DeleteAnimal(animal.Id);
            Assert.IsTrue(deletedAnimalId == animal.Id);
        }

        [TestMethod]
        public async Task UpdateAnimal_NewName_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            var updatedAnimal = await mAnimalService.UpdateAnimal(animal.Id, new UpdateAnimalRequest() { NewName = "Joe the cat" });
            Assert.IsTrue(updatedAnimal.Id == animal.Id);
            Assert.IsTrue(updatedAnimal.Name == "Joe the cat");

            var retrievedAnimal = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsTrue(retrievedAnimal.Id == animal.Id);
            Assert.IsTrue(retrievedAnimal.Name == "Joe the cat");
        }

        [TestMethod]
        public async Task UpdateAnimal_NoName_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.UpdateAnimal(animal.Id, new UpdateAnimalRequest() { NewName = "" }));
        }

        [TestMethod]
        public async Task UpdateAnimal_AnimalNotExist_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mAnimalService.UpdateAnimal("random", new UpdateAnimalRequest() { NewName = "Joe the cat" }));
        }

        [TestMethod]
        public async Task GetAnimals_Test()
        {
            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            var animal2 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal2);
            Assert.IsTrue(animal2.Name == "Jane the cat");
            Assert.IsTrue(animal2.TypeOfAnimal == AnimalType.Cat);

            var animal3 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Bobby the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal3);
            Assert.IsTrue(animal3.Name == "Bobby the cat");
            Assert.IsTrue(animal3.TypeOfAnimal == AnimalType.Cat);

            var retreivedAnimals = await mAnimalService.GetAnimals();
            var animals = retreivedAnimals.ToList();
            Assert.IsTrue(animals.Exists(x => x.Id == animal1.Id));
            Assert.IsTrue(animals.Exists(x => x.Id == animal2.Id));
            Assert.IsTrue(animals.Exists(x => x.Id == animal3.Id));
        }

        [TestMethod]
        public async Task GetAnimals_ByAnimalType_Test()
        {
            var animal1 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal1);
            Assert.IsTrue(animal1.Name == "Joe the dog");
            Assert.IsTrue(animal1.TypeOfAnimal == AnimalType.Dog);

            var animal2 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal2);
            Assert.IsTrue(animal2.Name == "Jane the cat");
            Assert.IsTrue(animal2.TypeOfAnimal == AnimalType.Cat);

            var animal3 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Bobby the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal3);
            Assert.IsTrue(animal3.Name == "Bobby the cat");
            Assert.IsTrue(animal3.TypeOfAnimal == AnimalType.Cat);

            var animal4 = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Leila the rabbit", TypeOfAnimal = AnimalType.Rabbit });
            Assert.IsNotNull(animal4);
            Assert.IsTrue(animal4.Name == "Leila the rabbit");
            Assert.IsTrue(animal4.TypeOfAnimal == AnimalType.Rabbit);

            var retreivedAnimals = await mAnimalService.GetAnimalsByType("cat");
            var animals = retreivedAnimals.ToList();
            Assert.IsFalse(animals.Exists(x => x.Id == animal1.Id));
            Assert.IsTrue(animals.Exists(x => x.Id == animal2.Id));
            Assert.IsTrue(animals.Exists(x => x.Id == animal3.Id));
            Assert.IsFalse(animals.Exists(x => x.Id == animal4.Id));
        }

        [TestMethod]
        public async Task GetAnimal_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Jane the cat", TypeOfAnimal = AnimalType.Cat });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Jane the cat");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Cat);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            var retrievedAnimal = await mAnimalService.GetAnimal(animal.Id);
            Assert.IsNotNull(retrievedAnimal);
            Assert.IsTrue(retrievedAnimal.Id == animal.Id);
            Assert.IsTrue(retrievedAnimal.Name == "Jane the cat");
            Assert.IsNull(retrievedAnimal.OwnerId);
        }

        [TestMethod]
        public async Task GetAnimal_AnimalNotExist_Test()
        {
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsNull(animal.OwnerId);
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);
            Assert.IsTrue(animal.HappinessLevel == 50);
            Assert.IsTrue(animal.HungerLevel == 50);
            Assert.IsTrue(animal.CleanlinessLevel == 50);

            await Assert.ThrowsExceptionAsync<NotFoundException>(() => mAnimalService.GetAnimal("random"));
        }

        [TestMethod]
        public void CalculateHappiness_Test()
        {
            DateTime now = DateTime.Now;

            Animal animal = new Animal()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Joe the dog",
                OwnerId = null,
                TypeOfAnimal = AnimalType.Dog,
                LastTimeFed = now,
                LastTimeStroked = now,
                LastTimeWashed = now,
                HungerValueStored = 20,
                HappinessValueStored = 20,
                CleanlinessValueStored = 20
            };

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(AnimalType.Dog);
            Assert.IsNotNull(animalTypeConfig);

            IAnimalStateChangeStrategy animalStateChangeStrategy = new AnimalStateStrategyPerSecond_Test(now);

            int happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 20);

            //Pretend 2 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 2));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 20);

            //Pretend another 2 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 2));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 19);

            //Pretend another 4 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 4));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 18);

            //Pretend another 12 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 12));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 15);

            //Pretend another 40 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 40));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 5);

            //Pretend another 2 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 2));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 5);

            //Pretend another 2 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 2));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 4);

            //Pretend another 16 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 16));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 0);

            //Pretend another 12 seconds have passed since the last time stroked
            animal.LastTimeStroked = animal.LastTimeStroked.Subtract(new TimeSpan(0, 0, 12));
            happinessLevel = animalStateChangeStrategy.CalculateHappinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(happinessLevel == 0);
        }

        [TestMethod]
        public void StrokeAnimal_Test()
        {
            DateTime now = DateTime.Now;

            Animal animal = new Animal()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Joe the dog",
                OwnerId = null,
                TypeOfAnimal = AnimalType.Dog,
                LastTimeFed = now,
                LastTimeStroked = now,
                LastTimeWashed = now,
                HungerValueStored = 60,
                HappinessValueStored = 60,
                CleanlinessValueStored = 60
            };

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);
            Assert.IsNotNull(animalTypeConfig);

            mAnimalService.StrokeAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HappinessValueStored == 68);

            mAnimalService.StrokeAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HappinessValueStored == 76);

            mAnimalService.StrokeAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HappinessValueStored == 84);

            mAnimalService.StrokeAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HappinessValueStored == 92);

            mAnimalService.StrokeAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HappinessValueStored == 100);

            mAnimalService.StrokeAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HappinessValueStored == 102);

            mAnimalService.StrokeAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HappinessValueStored == 102);
        }

        [TestMethod]
        public void CalculateHunger_Test()
        {
            DateTime now = DateTime.Now;

            Animal animal = new Animal()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Joe the dog",
                OwnerId = null,
                TypeOfAnimal = AnimalType.Dog,
                LastTimeFed = now,
                LastTimeStroked = now,
                LastTimeWashed = now,
                HungerValueStored = 20,
                HappinessValueStored = 20,
                CleanlinessValueStored = 20
            };

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(AnimalType.Dog);
            Assert.IsNotNull(animalTypeConfig);

            IAnimalStateChangeStrategy animalStateChangeStrategy = new AnimalStateStrategyPerSecond_Test(now);

            int hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 20);

            //Pretend 2 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 2));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 19);

            //Pretend 2 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 2));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 18);

            //Pretend 20 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 20));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 8);

            //Pretend 10 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 10));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 3);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 1));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 3);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 1));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 2);

            //Pretend 4 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 4));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 0);

            //Pretend 4 seconds have passed since the last time stroked
            animal.LastTimeFed = animal.LastTimeFed.Subtract(new TimeSpan(0, 0, 4));
            hungerLevel = animalStateChangeStrategy.CalculateHungerLevel(animal, animalTypeConfig);
            Assert.IsTrue(hungerLevel == 0);
        }

        [TestMethod]
        public void FeedAnimal_Test()
        {
            DateTime now = DateTime.Now;

            Animal animal = new Animal()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Joe the dog",
                OwnerId = null,
                TypeOfAnimal = AnimalType.Dog,
                LastTimeFed = now,
                LastTimeStroked = now,
                LastTimeWashed = now,
                HungerValueStored = 60,
                HappinessValueStored = 60,
                CleanlinessValueStored = 60
            };

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);
            Assert.IsNotNull(animalTypeConfig);

            mAnimalService.FeedAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HungerValueStored == 67);

            mAnimalService.FeedAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HungerValueStored == 74);

            mAnimalService.FeedAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HungerValueStored == 81);

            mAnimalService.FeedAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HungerValueStored == 88);

            mAnimalService.FeedAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HungerValueStored == 95);

            mAnimalService.FeedAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HungerValueStored == 102);

            mAnimalService.FeedAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.HungerValueStored == 102);
        }

        [TestMethod]
        public void CalculateCleanliness_Test()
        {
            DateTime now = DateTime.Now;

            Animal animal = new Animal()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Joe the dog",
                OwnerId = null,
                TypeOfAnimal = AnimalType.Dog,
                LastTimeFed = now,
                LastTimeStroked = now,
                LastTimeWashed = now,
                HungerValueStored = 20,
                HappinessValueStored = 20,
                CleanlinessValueStored = 20
            };

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(AnimalType.Dog);
            Assert.IsNotNull(animalTypeConfig);

            IAnimalStateChangeStrategy animalStateChangeStrategy = new AnimalStateStrategyPerSecond_Test(now);

            int cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 20);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 1));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 20);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 1));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 20);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 1));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 19);

            //Pretend 3 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 3));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 18);

            //Pretend 5 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 5));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 17);

            //Pretend 5 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 5));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 15);

            //Pretend 2 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 2));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 14);

            //Pretend 3 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 3));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 13);

            //Pretend 30 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 30));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 3);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 1));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 3);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 1));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 3);

            //Pretend 1 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 1));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 2);

            //Pretend 9 seconds have passed since the last time stroked
            animal.LastTimeWashed = animal.LastTimeWashed.Subtract(new TimeSpan(0, 0, 9));
            cleanlinessLevel = animalStateChangeStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);
            Assert.IsTrue(cleanlinessLevel == 0);
        }

        [TestMethod]
        public void WashAnimal_Test()
        {
            DateTime now = DateTime.Now;

            Animal animal = new Animal()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Joe the dog",
                OwnerId = null,
                TypeOfAnimal = AnimalType.Dog,
                LastTimeFed = now,
                LastTimeStroked = now,
                LastTimeWashed = now,
                HungerValueStored = 60,
                HappinessValueStored = 60,
                CleanlinessValueStored = 60
            };

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);
            Assert.IsNotNull(animalTypeConfig);

            mAnimalService.WashAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.CleanlinessValueStored == 71);

            mAnimalService.WashAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.CleanlinessValueStored == 82);

            mAnimalService.WashAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.CleanlinessValueStored == 93);

            mAnimalService.WashAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.CleanlinessValueStored == 102);

            mAnimalService.WashAnimal(animal, animalTypeConfig);
            Assert.IsTrue(animal.CleanlinessValueStored == 102);
        }
    }
}
