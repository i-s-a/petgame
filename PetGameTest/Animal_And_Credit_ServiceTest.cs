﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PetGame;
using PetGame.Requests;
using PetGame.Models;
using PetGame.Services;
using PetGame.Repositories;
using System.Threading.Tasks;

namespace PetGameTest
{
    [TestClass]
    public class Animal_And_Credit_ServiceTest
    {
        private UserService mUserService = null;
        private AnimalService mAnimalService = null;
        private CreditService mCreditService = null;
        private ConfigService mConfigService = null;
        private static bool mSetup = false;
        private DateTime mNow;

        [TestInitialize]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                .UseInMemoryDatabase(databaseName: "TestRepository_Animal_And_Credit_Service")
                .Options;

            mNow = DateTime.Now;
            IAnimalStateChangeStrategy animalStateChangeStrategy = new AnimalStateStrategyPerSecond_Test(mNow);

            ApiContext apiContext = new ApiContext(options);

            IConfigRepository configRepository = new InMemoryConfigRepository(apiContext);
            IUserRepository userRepository = new InMemoryUserRepository(apiContext);
            IAnimalRepository animalRepository = new InMemoryAnimalRepository(apiContext);

            mConfigService = new ConfigService(configRepository);
            mCreditService = new CreditService(new InMemoryCreditRepository(apiContext));
            mAnimalService = new AnimalService(animalRepository, animalStateChangeStrategy, mCreditService, mConfigService);
            mUserService = new UserService(userRepository, mAnimalService, mCreditService);

            if (!mSetup)
            {
                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Cat,
                    HappinessChangePerEvent = 2,
                    HappinessChangePerStroke = 6,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 1,
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 4,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 5,
                    CleanlinessChangePerEvent = 4,
                    CleanlinessChangePerWash = 8,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 8,
                    Description = "cat"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Dog,
                    HappinessChangePerEvent = 4,
                    HappinessChangePerStroke = 8,
                    HappinessMaxValue = 102,
                    HappinessCreditsRequiredForStroke = 1,
                    HungerChangePerEvent = 2,
                    HungerChangePerFeed = 7,
                    HungerMaxValue = 102,
                    HungerCreditsRequiredForFeed = 10,
                    CleanlinessChangePerEvent = 3,
                    CleanlinessChangePerWash = 11,
                    CleanlinessMaxValue = 102,
                    CleanlinessCreditsRequiredForWash = 12,
                    Description = "dog"
                });

                mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
                {
                    TypeOfAnimal = AnimalType.Rabbit,
                    HappinessChangePerEvent = 10,
                    HappinessChangePerStroke = 20,
                    HappinessMaxValue = 200,
                    HappinessCreditsRequiredForStroke = 1,
                    HungerChangePerEvent = 4,
                    HungerChangePerFeed = 8,
                    HungerMaxValue = 200,
                    HungerCreditsRequiredForFeed = 3,
                    CleanlinessChangePerEvent = 1,
                    CleanlinessChangePerWash = 4,
                    CleanlinessMaxValue = 200,
                    CleanlinessCreditsRequiredForWash = 5,
                    Description = "rabbit"
                });

                mSetup = true;
            }
        }

        [TestMethod]
        public async Task FeedAnimal_ExactCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, exactly enough to feed a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 10
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(10, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 10);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Feed the dog
            var feedResponse = await mAnimalService.FeedAnimal(new FeedAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            });

            credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

        }

        [TestMethod]
        public async Task FeedAnimal_MoreCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, more than enough to feed a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 11
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(11, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 11);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Feed the dog
            var feedResponse = await mAnimalService.FeedAnimal(new FeedAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            });

            credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 1);

        }

        [TestMethod]
        public async Task FeedAnimal_NotEnoughCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, but not enough to feed a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 9
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(9, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 9);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Feed the dog. We should get an ArgumentException because the user doesn't have enough
            //credit
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.FeedAnimal(new FeedAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            }));
        }

        [TestMethod]
        public async Task FeedAnimal_NoCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.FeedAnimal(new FeedAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            }));
        }

        [TestMethod]
        public async Task StrokeAnimal_ExactCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, exactly enough to stroke a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 1
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(1, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 1);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Stroke the dog
            var strokeResponse = await mAnimalService.StrokeAnimal(new StrokeAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            });

            credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

        }

        [TestMethod]
        public async Task StrokeAnimal_MoreCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, more than enough to stroke a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 2
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(2, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 2);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Stroke the dog
            var strokeResponse = await mAnimalService.StrokeAnimal(new StrokeAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            });

            credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 1);
        }

        [TestMethod]
        public async Task StrokeAnimal_NotEnoughCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Stroke the dog. We should get an ArgumentException because the user doesn't have enough
            //credit
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.StrokeAnimal(new StrokeAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            }));
        }

        [TestMethod]
        public async Task StrokeAnimal_NoCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.StrokeAnimal(new StrokeAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            }));
        }

        [TestMethod]
        public async Task WashAnimal_ExactCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, exactly enough to wash a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 12
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(12, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 12);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Wash the dog
            var washResponse = await mAnimalService.WashAnimal(new WashAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            });

            credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);
        }

        [TestMethod]
        public async Task WashAnimal_MoreCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, more than enough to wash a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 13
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(13, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 13);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Wash the dog
            var washResponse = await mAnimalService.WashAnimal(new WashAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            });

            credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 1);

        }

        [TestMethod]
        public async Task WashAnimal_NotEnoughCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            //Give the user some credit, but not enough to wash a dog
            var creditResponse = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 11
            });

            Assert.AreEqual(user.Id, creditResponse.Id);
            Assert.AreEqual(11, creditResponse.Amount);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 11);

            //Add a dog and associate the dog with the user
            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            //Wash the dog. We should get an ArgumentException because the user doesn't have enough
            //credit
            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.WashAnimal(new WashAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            }));
        }

        [TestMethod]
        public async Task WashAnimal_NoCredits_Test()
        {
            var user = await mUserService.AddUser(new NewUserRequest() { Username = "Luke Skywalker" });
            Assert.IsNotNull(user);
            Assert.IsTrue(user.Name == "Luke Skywalker");
            Assert.IsTrue(user.AnimalIds.Count == 0);

            var credit = await mCreditService.GetUserCredit(user.Id);
            Assert.IsNotNull(credit);
            Assert.AreEqual(credit.Amount, 0);

            var animal = await mAnimalService.AddAnimal(new NewAnimalRequest() { Name = "Joe the dog", TypeOfAnimal = AnimalType.Dog });
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "Joe the dog");
            Assert.IsTrue(animal.TypeOfAnimal == AnimalType.Dog);

            user = await mUserService.AssociateAnimalWithOwner(user.Id, animal.Id);
            Assert.IsTrue(user.AnimalIds.Exists(x => x == animal.Id));
            Assert.IsTrue(user.AnimalIds.Count == 1);

            await Assert.ThrowsExceptionAsync<ArgumentException>(() => mAnimalService.WashAnimal(new WashAnimalRequest()
            {
                UserId = user.Id,
                AnimalId = animal.Id
            }));
        }
    }
}