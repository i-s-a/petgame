﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using PetGame.Services;
using PetGame.Repositories;
using PetGame.Scheduler;

namespace PetGame
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase(databaseName: "PetGame"));
            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase(databaseName: "AnimalTypeConfig"));

            services.AddScoped<IUserRepository, InMemoryUserRepository>();
            services.AddScoped<IConfigRepository, InMemoryConfigRepository>();
            services.AddScoped<IAnimalRepository, InMemoryAnimalRepository>();
            services.AddScoped<IAnimalStateChangeStrategy, AnimalStateStrategyPerMinute>();
            services.AddScoped<ICreditRepository, InMemoryCreditRepository>();
            services.AddScoped<INotificationFactory, NotificationFactory>();
            services.AddScoped(typeof(UserService));
            services.AddScoped(typeof(AnimalService));
            services.AddScoped(typeof(ConfigService));
            services.AddScoped(typeof(CreditService));

            //-------------------------------------------------------------
            //FOR TESTING ONLY
            services.AddTransient<AnimalTypeConfigSamplePopulate>();
            //-------------------------------------------------------------

            services.AddMvc();

            //Add Swagger generation services to the container
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Pet Game API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, AnimalTypeConfigSamplePopulate animalTypeConfigSamplePopulate)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pet Game API v1");
            });

            //-------------------------------------------------------------
            //FOR TESTING ONLY
            animalTypeConfigSamplePopulate.Populate();
            //-------------------------------------------------------------
        }
    }
}
