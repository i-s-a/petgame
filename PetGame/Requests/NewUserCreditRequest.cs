﻿namespace PetGame.Requests
{
    public class NewUserCreditRequest
    {
        public string UserId { get; set; }

        //Generally new users start with 0, but they could start with a certain amount of credit (e.g. if part of a promotion for new users or something)
        public int Amount { get; set; }
    }

}
