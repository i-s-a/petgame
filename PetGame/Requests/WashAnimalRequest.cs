﻿namespace PetGame.Requests
{
    public class WashAnimalRequest
    {
        public string UserId { get; set; }

        public string AnimalId { get; set; }
    }
}
