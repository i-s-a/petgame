﻿namespace PetGame.Requests
{
    public class NewUserRequest
    {
        public string Username { get; set; }
    }

}
