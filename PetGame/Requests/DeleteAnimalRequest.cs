﻿namespace PetGame.Requests
{
    public class DeleteAnimalRequest
    {
        public string AnimalId { get; set; }
    }
}
