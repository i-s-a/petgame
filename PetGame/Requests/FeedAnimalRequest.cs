﻿namespace PetGame.Requests
{
    public class FeedAnimalRequest
    {
        public string UserId { get; set; }

        public string AnimalId { get; set; }
    }
    
}
