﻿namespace PetGame.Requests
{
    public class UpdateAnimalRequest
    {
        public string NewName { get; set; }
    }
}
