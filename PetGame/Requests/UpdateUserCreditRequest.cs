﻿namespace PetGame.Requests
{
    public class UpdateUserCreditRequest
    {
        public int Amount { get; set; }
    }
}
