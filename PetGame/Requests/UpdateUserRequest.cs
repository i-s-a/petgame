﻿namespace PetGame.Requests
{
    public class UpdateUserRequest
    {
        public string NewName { get; set; }
    }
}
