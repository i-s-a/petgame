﻿using System;
using PetGame.Models;

namespace PetGame
{

    public class AnimalStateStrategyPerMinute_Test : IAnimalStateChangeStrategy
    {
        private DateTime mTimeToUse;

        public AnimalStateStrategyPerMinute_Test(DateTime timeToUse)
        {
            mTimeToUse = timeToUse;
        }

        public float CalculateHappinessLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            float happinessLevel = CalculateHappinessLevel(animal, animalTypeConfig);

            //Calculate the happiness level as a percentage
            happinessLevel = (happinessLevel / animalTypeConfig.HappinessMaxValue) * 100;

            return happinessLevel;
        }

        public float CalculateHungerLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            float hungerLevel = CalculateHungerLevel(animal, animalTypeConfig);

            //Calculate the hunger level as a percentage
            hungerLevel = (hungerLevel / animalTypeConfig.HungerMaxValue) * 100;

            return hungerLevel;
        }

        public float CalculateCleanlinessLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            float cleanlinessLevel = CalculateCleanlinessLevel(animal, animalTypeConfig);

            //Calculate the hunger level as a percentage
            cleanlinessLevel = (cleanlinessLevel / animalTypeConfig.CleanlinessMaxValue) * 100;

            return cleanlinessLevel;
        }

        public int CalculateHappinessLevel(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            DateTime lastTimeStroked = animal.LastTimeStroked;

            DateTime now = mTimeToUse;
            TimeSpan timeDiff = now - lastTimeStroked;

            int change = (int)timeDiff.TotalMinutes / animalTypeConfig.HappinessChangePerEvent;

            int happinessLevel = animal.HappinessValueStored - change;

            if (happinessLevel < 0)
                happinessLevel = 0;

            return happinessLevel;
        }

        public int CalculateHungerLevel(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            DateTime lastTimeFed = animal.LastTimeFed;

            DateTime now = mTimeToUse;
            TimeSpan timeDiff = now - lastTimeFed;

            int change = (int)timeDiff.TotalMinutes / animalTypeConfig.HungerChangePerEvent;

            int hungerLevel = animal.HungerValueStored - change;

            if (hungerLevel < 0)
                hungerLevel = 0;

            return hungerLevel;
        }

        public int CalculateCleanlinessLevel(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            DateTime lastWashed = animal.LastTimeWashed;

            DateTime now = mTimeToUse;
            TimeSpan timeDiff = now - lastWashed;

            int change = (int)timeDiff.TotalMinutes / animalTypeConfig.CleanlinessChangePerEvent;

            int cleanlinessLevel = animal.CleanlinessValueStored - change;

            if (cleanlinessLevel < 0)
                cleanlinessLevel = 0;

            return cleanlinessLevel;
        }
    }

}
