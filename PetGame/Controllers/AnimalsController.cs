﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetGame.Requests;
using PetGame.Services;

namespace PetGame.Controllers
{
    //[Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class AnimalsController : Controller
    {
        private readonly AnimalService mAnimalService = null;
        private readonly ConfigService mConfigService = null;
        private readonly IAnimalStateChangeStrategy mAnimalStateStrategy = null;

        public AnimalsController(AnimalService animalService, ConfigService configService, IAnimalStateChangeStrategy animalStateStrategy)
        {
            mAnimalService = animalService;
            mConfigService = configService;

            mAnimalStateStrategy = animalStateStrategy;
        }

        // GET: api/v1/animals?type=cat
        [HttpGet]
        public async Task<IActionResult> GetAllAnimals(string type)
        {
            try
            {
                OkObjectResult okObjectResult = null;

                if (string.IsNullOrEmpty(type))
                {
                    var animalResponses = await mAnimalService.GetAnimals();
                    okObjectResult = new OkObjectResult(animalResponses);
                }
                else
                {
                    var animalResponses = await mAnimalService.GetAnimalsByType(type);
                    okObjectResult = new OkObjectResult(animalResponses);
                }

                return okObjectResult;

            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/v1/animals/5
        [HttpGet("{animalId}", Name = "GetAnimalById")]
        public async Task<IActionResult> GetAnimalById(string animalId)
        {
            try
            {
                var animalResponse = await mAnimalService.GetAnimal(animalId);

                return Ok(animalResponse);

            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST: api/v1/animals
        [HttpPost("create")]
        public async Task<IActionResult> CreateAnimal([FromBody]NewAnimalRequest request)
        {
            try
            {
                var animalResponse = await mAnimalService.AddAnimal(request);
                
                return Created("animals/" + animalResponse.Id.ToString(), animalResponse);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT: api/v1/animals/5
        // Updates the animal details. Currently only the name can be updated by this end point. The other Animal
        // details (e.g. happiness, hunger, etc.) are **never** updated by this end point, they only get updated
        // by the API internally and returned with the animal response. If other animal details are added in the
        // future, that can be updated by this end point, then these will be updated in full as well. PATCH can
        // be used to perform a partial update        
        [HttpPut("{animalId}")]
        public async Task<IActionResult> UpdateAnimal(string animalId, [FromBody]UpdateAnimalRequest request)
        {
            try
            {
                var animalResponse = await mAnimalService.UpdateAnimal(animalId, request);

                return Ok(animalResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE: api/v1/animals/5
        [HttpDelete("{animalId}")]
        public async Task<IActionResult> DeleteAnimal(string animalId)
        {
            try
            {
                var animalResponse = await mAnimalService.DeleteAnimal(animalId);
                
                return Ok(animalResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/v1/animals
        // Stroke a pet
        [HttpPost("stroke")]
        public async Task<IActionResult> StrokeAnimal([FromBody]StrokeAnimalRequest request)
        {
            try
            {
                var animalResponse = await mAnimalService.StrokeAnimal(request);

                return Ok(animalResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/v1/animals
        // Feed a pet
        [HttpPost("feed")]
        public async Task<IActionResult> FeedAnimal([FromBody]FeedAnimalRequest request)
        {
            try
            {
                var animalResponse = await mAnimalService.FeedAnimal(request);

                return Ok(animalResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/v1/animals
        // Wash a pet
        [HttpPost("wash")]
        public async Task<IActionResult> WashAnimal([FromBody]WashAnimalRequest request)
        {
            try
            {
                var animalResponse = await mAnimalService.WashAnimal(request);

                return Ok(animalResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
