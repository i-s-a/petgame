﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetGame.Requests;
using PetGame.Services;

namespace PetGame.Controllers
{
    //[Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class CreditsController : Controller
    {
        private readonly CreditService mCreditService = null;

        public CreditsController(CreditService creditService)
        {
            mCreditService = creditService;
        }

        // GET: api/v1/credits/5
        [HttpGet("{userId}", Name = "GetCreditById")]
        public async Task<IActionResult> GetCreditById(string userId)
        {
            try
            {
                var creditResponse = await mCreditService.GetUserCredit(userId);

                return Ok(creditResponse);

            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT: api/v1/credits/5
        [HttpPut("{userId}")]
        public async Task<IActionResult> CreateOrUpdateUserCredit(string userId, [FromBody]UpdateUserCreditRequest request)
        {
            try
            {
                var creditResponse = await mCreditService.CreateOrUpdateUserCredit(userId, request);

                return Created("credits/" + creditResponse.Id.ToString(), creditResponse);
            }
            catch(ArgumentOutOfRangeException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE: api/v1/credits/5?force=true
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUserCredit(string userId, bool? force)
        {
            try
            {
                bool forceDeleteIfCreditNotEmpty = (force.HasValue ? force.Value : false);

                var creditResponse = await mCreditService.DeleteUserCredit(userId, forceDeleteIfCreditNotEmpty);
                
                return Ok(creditResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
