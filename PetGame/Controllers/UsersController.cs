﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetGame.Requests;
using PetGame.Models;
using PetGame.Services;

namespace PetGame.Controllers
{
    [Route("api/v1/[controller]")]
    public class UsersController : Controller
    {
        private readonly UserService mUserService = null;
        private readonly ConfigService mConfigService = null;
        private readonly IAnimalStateChangeStrategy mAnimalStateStrategy = null;

        public UsersController(UserService userService, ConfigService configService, IAnimalStateChangeStrategy animalStateStrategy)
        {
            mUserService = userService;
            mConfigService = configService;

            mAnimalStateStrategy = animalStateStrategy;
        }

        // GET api/v1/users
        // Return a list of all users
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var userResponses = await mUserService.GetAllUsers();

                return Ok(userResponses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/v1/users/5
        // Get a specific user by ID
        [HttpGet("{userid}")]
        public async Task<IActionResult> GetUserById(string userId)
        {
            try
            {
                var userResponse = await mUserService.GetUser(userId);

                return Ok(userResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET api/v1/users/5/animals?type=cat
        // Return a list of all animals associated with a particular user, or if a
        // type specified, return a list of animals with a specific type associated
        // with a particular user.
        [HttpGet("{userid}/animals")]
        public async Task<IActionResult> GetAnimalsForUser(string userId, string type)
        {
            try
            {
                if (!string.IsNullOrEmpty(type))
                {
                    AnimalType animalType = AnimalHelper.GetAnimalTypeFromString(type);
                    var animalResponse = await mUserService.GetAnimals(userId, animalType);
                    return Ok(animalResponse);
                }
                else
                {
                    var animalResponse = await mUserService.GetAnimals(userId);
                    return Ok(animalResponse);
                }
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // Get api/v1/users/5/animals/1
        // Return a specific animal for a particular user
        [HttpGet("{userid}/animals/{animalid}")]
        public async Task<IActionResult> GetAnimalForUser(string userId, string animalId)
        {
            try
            {
                var animalResponse = await mUserService.GetAnimal(userId, animalId);

                return Ok(animalResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST api/v1/users
        // Create a user
        [HttpPost("create")]
        public async Task<IActionResult> CreateUser([FromBody]NewUserRequest request)
        {
            try
            {
                var userResponse = await mUserService.AddUser(request);

                //return CreatedAtRoute("api/v1/users/" + user.Id.ToString(), userResponse);
                return Created("users/" + userResponse.Id.ToString(), userResponse);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT api/v1/users/5
        // Update the user details (currently only support name). PUT will assume we are updating all the
        // user's details, and PATCH will assume we are doing a partial update of the user's details. At
        // the moment because we only support username PUT and PATCH are essentially the same thing. If
        // more details are added to the User in the future then the UpdateUserRequest should also be updated
        // with the details.
        [HttpPut("{userId}")]
        public async Task<IActionResult> UpdateUser(string userId, [FromBody]UpdateUserRequest request)
        {
            try
            {
                var userResponse = await mUserService.UpdateUser(userId, request);

                return Ok(userResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT api/v1/users/5/animals/2
        // Associate an animal with a particular user. After this call the user will own the animal
        [HttpPut("{userId}/animals/{animalId}")]
        public async Task<IActionResult> AssociateAnimalWithOwner(string userId, string animalId)
        {
            try
            {
                var userResponse = await mUserService.AssociateAnimalWithOwner(userId, animalId);

                return Created("users/" + userId.ToString(), userResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE api/v1/users/1/animals/1
        [HttpDelete("{userId}/animals/{animalId}")]
        public async Task<IActionResult> RemoveAnimalFromOwner(string userId, string animalId)
        {
            try
            {
                var userResponse = await mUserService.RemoveAnimalFromOwner(userId, animalId);

                return Ok(userResponse);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // DELETE api/v1/users/5
        // Delete a user and remove any associations they have with any animals
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            try
            {
                var user = await mUserService.DeleteUser(userId);

                return Ok(user);
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //We need a way to set the config for each animal type. Typically
        //this would be set up through some kind of administration system.
        //For now just make this method available!!!
        [ApiExplorerSettings(IgnoreApi = true)]
        public void AddAnimalConfigToDb(AnimalTypeConfig animalTypeConfig)
        {
            mConfigService.AddAnimalTypeConfig(animalTypeConfig);
        }
    }
}
