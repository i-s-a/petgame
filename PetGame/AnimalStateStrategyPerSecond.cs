﻿using System;
using PetGame.Models;

namespace PetGame
{
    public class AnimalStateStrategyPerSecond : IAnimalStateChangeStrategy
    {
        public float CalculateHappinessLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            float happinessLevel = CalculateHappinessLevel(animal, animalTypeConfig);

            //Calculate the happiness level as a percentage
            happinessLevel = (happinessLevel / animalTypeConfig.HappinessMaxValue) * 100;

            return happinessLevel;
        }

        public float CalculateHungerLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            float hungerLevel = CalculateHungerLevel(animal, animalTypeConfig);

            //Calculate the hunger level as a percentage
            hungerLevel = (hungerLevel / animalTypeConfig.HungerMaxValue) * 100;

            return hungerLevel;
        }

        public float CalculateCleanlinessLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            float cleanlinessLevel = CalculateCleanlinessLevel(animal, animalTypeConfig);

            //Calculate the hunger level as a percentage
            cleanlinessLevel = (cleanlinessLevel / animalTypeConfig.CleanlinessMaxValue) * 100;

            return cleanlinessLevel;
        }

        public int CalculateHappinessLevel(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            DateTime lastTimeStroked = animal.LastTimeStroked;

            DateTime now = DateTime.Now;
            TimeSpan timeDiff = now - lastTimeStroked;

            //HappinessChangePerEvent is the number of seconds it takes happiness to decrease 1 unit
            int change = (int)timeDiff.TotalSeconds / animalTypeConfig.HappinessChangePerEvent;

            int happinessLevel = animal.HappinessValueStored - change;

            if (happinessLevel < 0)
                happinessLevel = 0;

            return happinessLevel;
        }

        public int CalculateHungerLevel(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            DateTime lastTimeFed = animal.LastTimeFed;

            DateTime now = DateTime.Now;
            TimeSpan timeDiff = now - lastTimeFed;

            //HungerChangePerEvent is the number of seconds it takes hunger to increase 1 unit
            int change = (int)timeDiff.TotalSeconds / animalTypeConfig.HungerChangePerEvent;

            int hungerLevel = animal.HungerValueStored - change;

            if (hungerLevel < 0)
                hungerLevel = 0;

            return hungerLevel;
        }

        public int CalculateCleanlinessLevel(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            DateTime lastWashed = animal.LastTimeWashed;

            DateTime now = DateTime.Now;
            TimeSpan timeDiff = now - lastWashed;

            //CleanlinessChangePerEvent is the number of seconds it takes cleanliness to decrease 1 unit
            int change = (int)timeDiff.TotalSeconds / animalTypeConfig.CleanlinessChangePerEvent;

            int cleanlinessLevel = animal.CleanlinessValueStored - change;

            if (cleanlinessLevel < 0)
                cleanlinessLevel = 0;

            return cleanlinessLevel;
        }
    }
}
