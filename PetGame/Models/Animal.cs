﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PetGame.Models
{
    public class Animal
    {
        [Key]
        public string Id { get; set; }

        public string OwnerId { get; set; }

        public AnimalType TypeOfAnimal { get; set; }

        public string Name { get; set; }

        public int HappinessValueStored { get; set; }

        public int HungerValueStored { get; set; }

        public int CleanlinessValueStored { get; set; }

        public DateTime LastTimeStroked { get; set; }

        public DateTime LastTimeFed { get; set; }

        public DateTime LastTimeWashed { get; set; }
    }
}
