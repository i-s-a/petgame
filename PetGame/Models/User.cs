﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PetGame.Models
{
    public class User
    {
        [Key]
        public string Id { get; set; }

        public string Name { get; set; }

        public List<Animal_Id> AnimalIds { get; set; }
    }
}
