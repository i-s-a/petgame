﻿using System.ComponentModel.DataAnnotations;

namespace PetGame.Models
{
    public class Credit
    {
        [Key]
        public string Id { get; set; }	//UserId

        public int Amount { get; set; }	//Amount to be credited or debited. + means add credits, - means deduct credits
    }
}
