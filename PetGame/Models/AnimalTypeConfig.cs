﻿using System.ComponentModel.DataAnnotations;

namespace PetGame.Models
{
    public class AnimalTypeConfig
    {
        [Key]
        public AnimalType TypeOfAnimal { get; set; }

        public int HungerMaxValue { get; set; }

        public int HungerChangePerEvent { get; set; }

        public int HungerChangePerFeed { get; set; }

        public int HungerCreditsRequiredForFeed { get; set; }

        public int HappinessMaxValue { get; set; }

        public int HappinessChangePerEvent { get; set; }

        public int HappinessChangePerStroke { get; set; }

        public int HappinessCreditsRequiredForStroke { get; set; }

        public int CleanlinessMaxValue { get; set; }

        public int CleanlinessChangePerEvent { get; set; }

        public int CleanlinessChangePerWash { get; set; }

        public int CleanlinessCreditsRequiredForWash { get; set; }

        public string Description { get; set; }
    }
}
