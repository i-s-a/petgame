﻿using System.Collections.Generic;

namespace PetGame.Responses
{
    public class UserResponse
    {
        public string Id { set; get; }

        public string Name { set; get; }

        public List<string> AnimalIds { set; get; }
    }
}
