﻿namespace PetGame.Responses
{
    public class AnimalResponse
    {
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string Name { get; set; }
        public AnimalType TypeOfAnimal { get; set; }
        public float HappinessLevel { get; set; }
        public float HungerLevel { get; set; }
        public float CleanlinessLevel { get; set; }
    }
}
