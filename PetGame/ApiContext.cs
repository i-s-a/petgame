﻿using Microsoft.EntityFrameworkCore;
using PetGame.Models;

namespace PetGame
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<AnimalTypeConfig> AnimalConfig { get; set; }
        public DbSet<Credit> Credits { get; set; }
    }
}
