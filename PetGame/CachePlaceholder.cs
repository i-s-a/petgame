﻿using System;
using System.Threading.Tasks;

namespace PetGame
{
    public class CachePlaceholder<K, T> : ICache<K, T>
    {
        public void Add(K key, T item)
        {
            throw new NotImplementedException();
        }

        public void Add(K key, T item, long durationInMilliseconds)
        {
            throw new NotImplementedException();
        }

        public void Add(K key, T item, DateTimeOffset expiration)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(K key, T item, long durationInMilliseconds)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(K key, T item, DateTimeOffset expiration)
        {
            throw new NotImplementedException();
        }

        public void Delete(K key)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(K key)
        {
            throw new NotImplementedException();
        }

        public bool Exists(K key)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ExistsAsync(K key)
        {
            throw new NotImplementedException();
        }

        public T Get(K key)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetAsync(K key)
        {
            throw new NotImplementedException();
        }

        public void Update(K key, T item)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(K key, T item)
        {
            throw new NotImplementedException();
        }
    }
}
