﻿using System.ComponentModel.DataAnnotations;

namespace PetGame
{
    /// <summary>
    /// We can't just add a list of strings to the User class because EF
    /// will try to create a table of strings which it can't do. Therefore
    /// we need to wrap the Id in a class with which EF can create a table
    /// and make the foreign key relationships
    /// </summary>
    public class Animal_Id
    {
        [Key]
        public string Id { get; set; }
    }
}
