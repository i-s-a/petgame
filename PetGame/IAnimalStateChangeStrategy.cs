﻿using PetGame.Models;

namespace PetGame
{
    public interface IAnimalStateChangeStrategy
    {
        /// <summary>
        /// Using the animal type configuration, calculate the happiness level of an animal
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="animalTypeConfig"></param>
        /// <returns>A value indicating how happy the animal is. 0 is extremley sad and HappinessMaxValue extremley happy</returns>
        int CalculateHappinessLevel(Animal animal, AnimalTypeConfig animalTypeConfig);

        /// <summary>
        /// Using the animal type configuration, calculate the happiness level of an animal as a percentage
        /// </summary>
        /// <param name="animal">The animal whose happiness will be calculated</param>
        /// <param name="animalTypeConfig">The configuration of the animal type</param>
        /// <returns>A percentage indicating how happy the animal is. 0 is extremley sad and 100 extremley happy</returns>
        float CalculateHappinessLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig);

        /// <summary>
        /// Using the animal type configuration, calculate the hunger level of an animal
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="animalTypeConfig"></param>
        /// <returns>A value indicating how hungry the animal is. 0 is starving and HungerMaxValue totally full</returns>
        int CalculateHungerLevel(Animal animal, AnimalTypeConfig animalTypeConfig);

        /// <summary>
        /// Using the animal type configuration, calculate the hunger level of an animal as a percentage
        /// </summary>
        /// <param name="animal">The animal whose hunger level will be calculated</param>
        /// <param name="animalTypeConfig">The configuration of the animal type</param>
        /// <returns>A percentage indicating how hungry the animal is. 0 is starving and 100 totally full</returns>
        float CalculateHungerLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig);

        /// <summary>
        /// Using the animal type configuration, calculate the cleanliness level of an animal
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="animalTypeConfig"></param>
        /// <returns>A value indicating how clean the animal is. 0 is very dirty and CleanlinessMaxValue totally clean</returns>
        int CalculateCleanlinessLevel(Animal animal, AnimalTypeConfig animalTypeConfig);

        /// <summary>
        /// Using the animal type configuration, calculate the cleanliness level of an animal as a percentage
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="animalTypeConfig"></param>
        /// <returns>A percentage indicating how clean the animal is. 0 is very dirty and 100 totally clean</returns>
        float CalculateCleanlinessLevelAsPercentage(Animal animal, AnimalTypeConfig animalTypeConfig);
    }
}
