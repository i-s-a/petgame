﻿using System;
using System.Threading.Tasks;

namespace PetGame
{
    public interface ICache<K, T>
    {
        /// <summary>
        /// Add an item, indexed by key, to the cache
        /// </summary>
        /// <param name="key">The key of the item to add</param>
        /// <param name="item">The item to add</param>
        void Add(K key, T item);

        /// <summary>
        /// Add an item, indexed by key, to the cache. The item expires and is automatically removed from the cache
        /// after a certain number of milliseconds
        /// </summary>
        /// <param name="key">The key of the item to add</param>
        /// <param name="item">The item to add</param>
        /// <param name="duration">The number of milliseconds after which the item is automatically removed</param>
        void Add(K key, T item, long durationInMilliseconds);
        Task AddAsync(K key, T item, long durationInMilliseconds);

        /// <summary>
        /// Add an item, indexed by key, to the cache. The item expires and is automatically removed at a specific time
        /// </summary>
        /// <param name="key">The key of the item to add</param>
        /// <param name="item">The item to add</param>
        /// <param name="expiration">The specific time after which the item is automatically removed</param>
        void Add(K key, T item, DateTimeOffset expiration);
        Task AddAsync(K key, T item, DateTimeOffset expiration);

        /// <summary>
        /// Remove an item, indexed by key, from the cache
        /// </summary>
        /// <param name="key">The key of the item to be removed</param>
        void Delete(K key);
        Task DeleteAsync(K key);

        /// <summary>
        /// Update an item, indexed by key, in the cache
        /// </summary>
        /// <param name="key">The key of the item to be updated</param>
        /// <param name="item">The updated item</param>
        void Update(K key, T item);
        Task UpdateAsync(K key, T item);

        /// <summary>
        /// Check if a key exists in the cache
        /// </summary>
        /// <param name="key">The key for which to search</param>
        /// <returns>True if the key exists, false if it doesn't exist</returns>
        bool Exists(K key);
        Task<bool> ExistsAsync(K key);

        /// <summary>
        /// Retrieve an item, indexed by key, from the cache
        /// </summary>
        /// <param name="key">The key of the itme to retrieve</param>
        /// <returns>The item, indexed by key, in the cache</returns>
        T Get(K key);
        Task<T> GetAsync(K key);
    }
}
