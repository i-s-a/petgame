﻿using PetGame.Services;
using PetGame.Models;

namespace PetGame
{
    public class AnimalTypeConfigSamplePopulate
    {
        private readonly ConfigService mConfigService = null;

        public AnimalTypeConfigSamplePopulate(ConfigService configService)
        {
            mConfigService = configService;
        }

        public void Populate()
        {
            mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
            {
                TypeOfAnimal = AnimalType.Cat,
                HappinessChangePerEvent = 2,
                HappinessChangePerStroke = 6,
                HappinessMaxValue = 200,
                HungerChangePerEvent = 2,
                HungerChangePerFeed = 4,
                HungerMaxValue = 200,
                CleanlinessChangePerEvent = 4,
                CleanlinessChangePerWash = 8,
                CleanlinessMaxValue = 200,
                Description = "cat"
            });

            mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
            {
                TypeOfAnimal = AnimalType.Dog,
                HappinessChangePerEvent = 4,
                HappinessChangePerStroke = 8,
                HappinessMaxValue = 102,
                HungerChangePerEvent = 2,
                HungerChangePerFeed = 7,
                HungerMaxValue = 102,
                CleanlinessChangePerEvent = 3,
                CleanlinessChangePerWash = 11,
                CleanlinessMaxValue = 102,
                Description = "dog"
            });

            mConfigService.AddAnimalTypeConfig(new AnimalTypeConfig()
            {
                TypeOfAnimal = AnimalType.Rabbit,
                HappinessChangePerEvent = 10,
                HappinessChangePerStroke = 20,
                HappinessMaxValue = 200,
                HungerChangePerEvent = 4,
                HungerChangePerFeed = 8,
                HungerMaxValue = 200,
                CleanlinessChangePerEvent = 1,
                CleanlinessChangePerWash = 4,
                CleanlinessMaxValue = 200,
                Description = "rabbit"
            });

        }
    }
}
