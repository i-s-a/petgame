﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetGame.Models;
using PetGame.Requests;
using PetGame.Responses;
using PetGame.Repositories;

namespace PetGame.Services
{
    public class UserService
    {
        private readonly IUserRepository mUserRepository = null;
        private readonly AnimalService mAnimalService = null;
        private readonly CreditService mCreditService = null;

        public UserService(IUserRepository userRepository, AnimalService animalService, CreditService creditService)
        {
            mUserRepository = userRepository;
            mAnimalService = animalService;
            mCreditService = creditService;
        }

        public async Task<UserResponse> AddUser(NewUserRequest request)
        {
            //We don't allow users with empty names
            if (request.Username == "")
                throw new ArgumentException("Attempting to create a user with no name");

            User user = new User()
            {
                Id = Guid.NewGuid().ToString(),
                Name = request.Username,
                AnimalIds = new List<Animal_Id>()
            };

            //Create an account for the user. Assume that it will always be empty when we first create it.
            //We may want, in the future, to create an account for a new user with some initial promotional
            //credits, in which case we will need to update the NewUserRequest with the amount of credits
            //with which the the user should start
            var _ = await mCreditService.CreateOrUpdateUserCredit(user.Id, new UpdateUserCreditRequest()
            {
                Amount = 0
            });

            await mUserRepository.AddAsync(user);

            UserResponse userResponse = GetUserResponse(user);

            return userResponse;
        }

        public async Task<string> DeleteUser(string userId)
        {
            User user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            //Delete the user's account. For now we don't allow a user to be deleted if they still
            //have credit in their account. There is an option to force delete the user even if they
            //have credit in their account but we don't currently allow that through the user service.
            //You would have to call the credit service directly to do that
            try
            {
                var _ = await mCreditService.DeleteUserCredit(userId, false);
            }
            catch (NotFoundException /*ex*/)
            {
                //Ignore if we don't find an account for the user as the account
                //could've been deleted by calling the credit service directly
            }

            //When we delete a user, should we also completely delete their pets or just remove the user
            //as an owner of the pets? Currently we just remove the user as an owner so that the pet has
            //no owner.
            //TODO: When we change to a microservices architecture, use the Event Sourcing pattern and publish
            //an AnimalRemovedFromOwner event to a message broker. The AnimalService should subscribe to receive
            //the event and update the animal in its internal store to no longer have an owner. The method
            //RemoveAnimalFromOwner(...) further down would publish the same event
            if (user.AnimalIds != null)
            {
                foreach (var animalId in user.AnimalIds)
                {
                    await mAnimalService.RemoveOwnerFromAnimal(animalId.Id, user.Id);
                }
            }

            await mUserRepository.DeleteAsync(user);

            return user.Id;
        }

        public async Task<UserResponse> UpdateUser(string userId, UpdateUserRequest request)
        {
            //We don't allow users with empty names
            if (request.NewName == "")
                throw new ArgumentException("Attempting to update with a user with no name");

            User user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            user.Name = request.NewName;

            await mUserRepository.UpdateUserAsync(user);

            var userResponse = GetUserResponse(user);

            return userResponse;
        }

        public async Task<UserResponse> GetUser(string userId)
        {
            var user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            var userResponse = GetUserResponse(user);

            return userResponse;
        }

        public async Task<IEnumerable<UserResponse>> GetAllUsers()
        {
            var users = await mUserRepository.GetAllUsersAsync();

            var usersResponse = GetUserResponseList(users);

            return usersResponse;
        }

        public async Task<AnimalResponse> GetAnimal(string userId, string animalId)
        {
            var user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            bool found = user.AnimalIds.Exists(x => x.Id == animalId);

            if (!found)
                throw new ArgumentException("Animal not associated with user");

            var animal = await mAnimalService.GetAnimal(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            return animal;
        }

        public async Task<IEnumerable<AnimalResponse>> GetAnimals(string userId)
        {
            var user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            var animalsResponse = await mAnimalService.GetAnimals(user.AnimalIds);

            return animalsResponse;
        }

        public async Task<IEnumerable<AnimalResponse>> GetAnimals(string userId, AnimalType animalType)
        {
            var user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            var animalsResponse = await mAnimalService.GetAnimals(user.AnimalIds, animalType);

            return animalsResponse;
        }

        public async Task<UserResponse> AssociateAnimalWithOwner(string userId, string animalId)
        {
            var user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            bool exists = user.AnimalIds.Exists(x => x.Id == animalId);

            if (exists)
                throw new ArgumentException("Animal already associated with user");

            var animal = await mAnimalService.GetAnimal(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");


            ////////////////////////////////////
            //TODO: When we change to a microservices architecture, use the Event Sourcing
            //pattern and publish an AnimalAssociatedWithOwner event to a message broker.
            //The AnimalService should subscribe to receive the event and update the animal
            //in its internal store with an owner.

            //Associate the animal with the user
            user.AnimalIds.Add(new Animal_Id() { Id = animalId });
            await mUserRepository.UpdateUserAsync(user);

            //Associate the user as the owner of the animal.
            await mAnimalService.AssociateOwnerWithAnimal(animalId, userId);
            ////////////////////////////////////

            var userResponse = GetUserResponse(user);

            return userResponse;
        }

        //Removes the animal from its internal store. It also informs the animal service
        public async Task<UserResponse> RemoveAnimalFromOwner(string userId, string animalId)
        {
            var user = await mUserRepository.GetUserAsync(userId);

            if (user == null)
                throw new NotFoundException("User not found");

            Animal_Id animal_id = user.AnimalIds.SingleOrDefault(x => x.Id == animalId);

            if (animal_id == null)
                throw new NotFoundException("Animal not assoociated with user");

            user.AnimalIds.Remove(animal_id);

            ////////////////////////////////////////
            //TODO: When we change to a microservices architecture, use the Event Sourcing
            //pattern and publish an AnimalRemovedFromOwner event to a message broker. The
            //AnimalService should subscribe to receive the event and update the animal in
            //its internal store so that it no longer has an owner. The method DeleteUser
            //further above would publish the same event
            await mUserRepository.UpdateUserAsync(user);

            //If RemoveOwnerFromAnimal throws an exception because the animal has a different owner
            //then we will still have already removed the animal from the list of animals associated
            //with the user but the exception will propagate to the controller and we will return to
            //the client a BadRequest error. 
            await mAnimalService.RemoveOwnerFromAnimal(animalId, userId);
            ////////////////////////////////////////

            var userResponse = GetUserResponse(user);

            return userResponse;
        }

        private List<UserResponse> GetUserResponseList(IEnumerable<User> users)
        {
            List<UserResponse> userResponses = new List<UserResponse>(users.Count());
            foreach (var user in users)
            {
                UserResponse userResponse = GetUserResponse(user);
                userResponses.Add(userResponse);
            }

            return userResponses;
        }

        private UserResponse GetUserResponse(User user)
        {
            List<string> animalIds = null;
            if (user.AnimalIds != null)
            {
                animalIds = new List<string>(user.AnimalIds.Count());
                foreach (var animalId in user.AnimalIds)
                {
                    animalIds.Add(animalId.Id);
                }
            }

            return new UserResponse()
            {
                Id = user.Id,
                Name = user.Name,
                AnimalIds = animalIds
            };
        }

    }
}
