﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetGame.Models;
using PetGame.Requests;
using PetGame.Responses;
using PetGame.Repositories;

namespace PetGame.Services
{
    public class CreditService
    {
        private readonly ICreditRepository mCreditRepository = null;

        public CreditService(ICreditRepository creditRepository)
        {
            mCreditRepository = creditRepository;
        }

        public async Task<CreditResponse> CreateOrUpdateUserCredit(string userId, UpdateUserCreditRequest request)
        {
            var credit = await mCreditRepository.GetAsync(userId);

            if (credit == null)
            {
                //User doesn't already have a credit account, so create one
                Credit newCredit = new Credit()
                {
                    Id = userId,
                    Amount = request.Amount
                };

                await mCreditRepository.AddAsync(newCredit);

                var creditResponse = GetCreditResponse(newCredit);

                return creditResponse;
            }
            else
            {
                //User already has a credit account, so update it

                //NOTE: We always add the amount from the request. If the amount is a positive number the account
                //will be credited, if the amount is a negative number the account will be debited
                int newAmount = credit.Amount + request.Amount;

                if (newAmount < 0)
                {
                    //We are debiting the account and the user doesn't have enough credits to deduct the amount requested.
                    //Don't allow the update. Use ArgumentOutOfRangeException exception for now.
                    throw new ArgumentOutOfRangeException("User doesn't have enough credits");
                }

                credit.Amount = newAmount;

                await mCreditRepository.UpdateAsync(credit);

                var creditResponse = GetCreditResponse(credit);

                return creditResponse;
            }
        }

        public async Task<CreditResponse> AddUserCredit(NewUserCreditRequest request)
        {
            if (request.UserId == "")
                throw new ArgumentException("Attempting to create a new user credit with no user ID");

            var currentUserCredit = await mCreditRepository.GetAsync(request.UserId);

            if (currentUserCredit != null)
                throw new AlreadyExistsException("User already has a credit account");
            
            Credit credit = new Credit()
            {
                Id = request.UserId,
                Amount = request.Amount
            };

            await mCreditRepository.AddAsync(credit);

            var creditResponse = GetCreditResponse(credit);

            return creditResponse;
        }

        public async Task<string> DeleteUserCredit(string userId, bool forceDeleteIfCreditNotEmpty)
        {
            var credit = await mCreditRepository.GetAsync(userId);

            if (credit == null)
                throw new NotFoundException("User credit not found");

            //If the account is not empty we only delete if we've been told to force the deletion through
            if (credit.Amount != 0 && !forceDeleteIfCreditNotEmpty)
                throw new ArgumentException("The user still has credit in their account");

            await mCreditRepository.DeleteAsync(credit);

            return credit.Id;
        }

        public async Task<CreditResponse> GetUserCredit(string userId)
        {
            var credit = await mCreditRepository.GetAsync(userId);

            if (credit == null)
                throw new NotFoundException("User credit not found");

            var creditResponse = GetCreditResponse(credit);

            return creditResponse;
        }

        public async Task<CreditResponse> UpdateUserCredit(string userId, UpdateUserCreditRequest request)
        {
            var credit = await mCreditRepository.GetAsync(userId);

            if (credit == null)
                throw new NotFoundException("User credit not found");

            //NOTE: We always add the amount from the request. If the amount is a positive number the account
            //will be credited, if the amount is a negative number the account will be debited
            int newAmount = credit.Amount + request.Amount;
            
            if (newAmount < 0)
            {
                //We are debiting the account and the user doesn't have enough credits to deduct the amount requested.
                //Don't allow the update. Use ArgumentOutOfRangeException exception for now. Change this later!!
                throw new ArgumentOutOfRangeException("User doesn't have enough credits");
            }

            credit.Amount = newAmount;

            await mCreditRepository.UpdateAsync(credit);

            var creditResponse = GetCreditResponse(credit);

            return creditResponse;
        }

        private CreditResponse GetCreditResponse(Credit credit)
        {
            return new CreditResponse()
            {
                Id = credit.Id,
                Amount = credit.Amount
            };
        }

        public async Task<bool> HasEnoughCredits(string userId, int creditsRequired)
        {
            var credit = await mCreditRepository.GetAsync(userId);

            if (credit == null)
                throw new NotFoundException("User credit not found");

            //If we subtract the credits required from the current amount and get a
            //negative amount then the user doesn't have enough credits
            if (credit.Amount - creditsRequired < 0)
                return false;

            return true;
        }
    }
}
