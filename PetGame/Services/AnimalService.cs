﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetGame.Models;
using PetGame.Requests;
using PetGame.Responses;
using PetGame.Repositories;

namespace PetGame.Services
{
    public class AnimalService
    {
        private readonly IAnimalRepository mAnimalRepository = null;
        private readonly ConfigService mConfigService = null;
        private readonly IAnimalStateChangeStrategy mAnimalStateStrategy = null;
        private readonly CreditService mCreditService = null;

        public AnimalService(IAnimalRepository animalRepository, IAnimalStateChangeStrategy animalStateStrategy,
            CreditService creditService, ConfigService configService)
        {
            mAnimalRepository = animalRepository;
            mCreditService = creditService;
            mConfigService = configService;
            mAnimalStateStrategy = animalStateStrategy;
        }

        public async Task<AnimalResponse> AddAnimal(NewAnimalRequest request)
        {
            //We don't allow animals with empty names
            if (request.Name == "")
                throw new ArgumentException("Attempting to create a animal with no name");

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(request.TypeOfAnimal);

            if (animalTypeConfig == null)
                throw new Exception("Attempting to create an animal with no type configuration");

            int happinessValue = animalTypeConfig.HappinessMaxValue / 2;
            int hungerValue = animalTypeConfig.HungerMaxValue / 2;
            int cleanlinessValue = animalTypeConfig.CleanlinessMaxValue / 2;

            Animal animal = new Animal()
            {
                Id = Guid.NewGuid().ToString(),
                OwnerId = null,
                TypeOfAnimal = request.TypeOfAnimal,
                Name = request.Name,
                HappinessValueStored = happinessValue,
                HungerValueStored = hungerValue,
                CleanlinessValueStored = cleanlinessValue,
                LastTimeStroked = DateTime.Now,
                LastTimeFed = DateTime.Now,
                LastTimeWashed = DateTime.Now
            };

            await mAnimalRepository.AddAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public async Task<AnimalResponse> UpdateAnimal(string animalId, UpdateAnimalRequest request)
        {
            if (request.NewName == "")
                throw new ArgumentException("Attempting to update with an animamal with no name");

            Animal animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            animal.Name = request.NewName;

            await mAnimalRepository.UpdateAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public async Task<AnimalResponse> GetAnimal(string animalId)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public async Task<IEnumerable<AnimalResponse>> GetAnimals()
        {
            var animals = await mAnimalRepository.GetAllAnimalsAsync();

            var animalsResponse = GetAnimalResponseList(animals);

            return animalsResponse;
        }

        public async Task<IEnumerable<AnimalResponse>> GetAnimalsByType(string type)
        {
            AnimalType animalType = AnimalHelper.GetAnimalTypeFromString(type);

            var animals = await mAnimalRepository.GetAllAnimalsByTypeAsync(animalType);

            var animalsResponse = GetAnimalResponseList(animals);

            return animalsResponse;
        }

        public async Task<IEnumerable<AnimalResponse>> GetAnimals(IEnumerable<Animal_Id> animalIds)
        {
            List<Animal> animals = new List<Animal>(animalIds.Count());
            foreach(var animalId in animalIds)
            {
                Animal animal = await mAnimalRepository.GetAnimalAsync(animalId.Id);
                animals.Add(animal);
            }

            var animalsResponse = GetAnimalResponseList(animals);

            return animalsResponse;
        }

        public async Task<IEnumerable<AnimalResponse>> GetAnimals(IEnumerable<Animal_Id> animalIds, AnimalType animalType)
        {
            List<Animal> animals = new List<Animal>(animalIds.Count());
            foreach (var animalId in animalIds)
            {
                Animal animal = await mAnimalRepository.GetAnimalAsync(animalId.Id);
                if (animal.TypeOfAnimal == animalType)
                {
                    animals.Add(animal);
                }
            }

            var animalsResponse = GetAnimalResponseList(animals);

            return animalsResponse;
        }

        public async Task<AnimalResponse> FeedAnimal(FeedAnimalRequest request)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(request.AnimalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            //When interacting with an animal through the AnimalsController we currently only allow users to
            //interact with animals they own.
            if (animal.OwnerId != request.UserId)
                throw new ArgumentException("Animal is not owned by the user");
            
            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);

            //If we can't find a animal type configuration then we can't proceed until
            //one has been added
            if (animalTypeConfig == null)
                throw new Exception("No config file found for animal type");

            //Does the user have enough credits to feed the animal
            bool enoughCredits = await mCreditService.HasEnoughCredits(animal.OwnerId, animalTypeConfig.HungerCreditsRequiredForFeed);
            if (!enoughCredits)
                throw new ArgumentException("User doesn't have enough credits for a feed");

            //Debit the user's credit account
            var _ = await mCreditService.CreateOrUpdateUserCredit(animal.OwnerId, new UpdateUserCreditRequest()
            {
                Amount = -animalTypeConfig.HungerCreditsRequiredForFeed
            });

            FeedAnimal(animal, animalTypeConfig);

            await mAnimalRepository.UpdateAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public async Task<AnimalResponse> FeedAnimal(string animalId)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);

            //If we can't find a animal type configuration then we can't proceed until
            //one has been added
            if (animalTypeConfig == null)
                throw new Exception("No config file found for animal type");

            //Does the user have enough credits to feed the animal
            bool enoughCredits = await mCreditService.HasEnoughCredits(animal.OwnerId, animalTypeConfig.HungerCreditsRequiredForFeed);
            if (!enoughCredits)
                throw new ArgumentException("User doesn't have enough credits for a feed");

            //Debit the user's credit account
            var _ = await mCreditService.CreateOrUpdateUserCredit(animal.OwnerId, new UpdateUserCreditRequest()
            {
                Amount = -animalTypeConfig.HungerCreditsRequiredForFeed
            });

            FeedAnimal(animal, animalTypeConfig);

            await mAnimalRepository.UpdateAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public void FeedAnimal(Animal animal, AnimalTypeConfig animalTypeConfig)
        {        
            int currentHungerLevel = mAnimalStateStrategy.CalculateHungerLevel(animal, animalTypeConfig);

            //Adjust the hunger level stored in the repository for the animal to take in to account that
            //it has been fed
            animal.HungerValueStored = currentHungerLevel + animalTypeConfig.HungerChangePerFeed;

            //If the hunger level exceeds the maximum allowed then, just for now, clamp the value
            //at the maximum
            if (animal.HungerValueStored > animalTypeConfig.HungerMaxValue)
                animal.HungerValueStored = animalTypeConfig.HungerMaxValue;

            animal.LastTimeFed = DateTime.Now;
        }

        public async Task<AnimalResponse> StrokeAnimal(StrokeAnimalRequest request)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(request.AnimalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            //When interacting with an animal through the AnimalsController we currently only allow users to
            //interact with animals they own.
            if (animal.OwnerId != request.UserId)
                throw new ArgumentException("Animal is not owned by the user");

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);

            //If we can't find a animal type configuration then we can't proceed until
            //one has been added
            if (animalTypeConfig == null)
                throw new Exception("No config file found for animal type");

            //Does the user have enough credits to stroke the animal
            bool enoughCredits = await mCreditService.HasEnoughCredits(animal.OwnerId, animalTypeConfig.HappinessCreditsRequiredForStroke);
            if (!enoughCredits)
                throw new ArgumentException("User doesn't have enough credits for a stroke");

            //Debit the user's credit account
            var _ = await mCreditService.CreateOrUpdateUserCredit(animal.OwnerId, new UpdateUserCreditRequest()
            {
                Amount = -animalTypeConfig.HappinessCreditsRequiredForStroke
            });

            StrokeAnimal(animal, animalTypeConfig);

            await mAnimalRepository.UpdateAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public async Task<AnimalResponse> StrokeAnimal(string animalId)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);

            //If we can't find a animal type configuration then we can't proceed until
            //one has been added
            if (animalTypeConfig == null)
                throw new Exception("No config file found for animal type");

            //Does the user have enough credits to stroke the animal
            bool enoughCredits = await mCreditService.HasEnoughCredits(animal.OwnerId, animalTypeConfig.HappinessCreditsRequiredForStroke);
            if (!enoughCredits)
                throw new ArgumentException("User doesn't have enough credits for a stroke");

            //Debit the user's credit account
            var _ = await mCreditService.CreateOrUpdateUserCredit(animal.OwnerId, new UpdateUserCreditRequest()
            {
                Amount = -animalTypeConfig.HappinessCreditsRequiredForStroke
            });

            StrokeAnimal(animal, animalTypeConfig);

            await mAnimalRepository.UpdateAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public void StrokeAnimal(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            int currentHappinessLevel = mAnimalStateStrategy.CalculateHappinessLevel(animal, animalTypeConfig);

            //Adjust the happiness level stored in the repository for the animal to take in to account that
            //it has been stroked
            animal.HappinessValueStored = currentHappinessLevel + animalTypeConfig.HappinessChangePerStroke;

            //If the happiness level exceeds the maximum allowed then, just for now, clamp the value
            //at the maximum
            if (animal.HappinessValueStored > animalTypeConfig.HappinessMaxValue)
                animal.HappinessValueStored = animalTypeConfig.HappinessMaxValue;

            animal.LastTimeStroked = DateTime.Now;
        }

        public async Task<AnimalResponse> WashAnimal(WashAnimalRequest request)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(request.AnimalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            //When interacting with an animal through the AnimalsController we currently only allow users to
            //interact with animals they own.
            if (animal.OwnerId != request.UserId)
                throw new ArgumentException("Animal is not owned by the user");

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);

            //If we can't find a animal type configuration then we can't proceed until
            //one has been added
            if (animalTypeConfig == null)
                throw new Exception("No config file found for animal type");

            //Does the user have enough credits to wash the animal
            bool enoughCredits = await mCreditService.HasEnoughCredits(animal.OwnerId, animalTypeConfig.CleanlinessCreditsRequiredForWash);
            if (!enoughCredits)
                throw new ArgumentException("User doesn't have enough credits for a wash");

            //Debit the user's credit account
            var _ = await mCreditService.CreateOrUpdateUserCredit(animal.OwnerId, new UpdateUserCreditRequest()
            {
                Amount = -animalTypeConfig.CleanlinessCreditsRequiredForWash
            });

            WashAnimal(animal, animalTypeConfig);

            await mAnimalRepository.UpdateAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public async Task<AnimalResponse> WashAnimal(string animalId)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);

            //If we can't find a animal type configuration then we can't proceed until
            //one has been added
            if (animalTypeConfig == null)
                throw new Exception("No config file found for animal type");

            //Does the user have enough credits to wash the animal
            bool enoughCredits = await mCreditService.HasEnoughCredits(animal.OwnerId, animalTypeConfig.CleanlinessCreditsRequiredForWash);
            if (!enoughCredits)
                throw new ArgumentException("User doesn't have enough credits for a wash");

            //Debit the user's credit account
            var _ = await mCreditService.CreateOrUpdateUserCredit(animal.OwnerId, new UpdateUserCreditRequest()
            {
                Amount = -animalTypeConfig.CleanlinessCreditsRequiredForWash
            });

            WashAnimal(animal, animalTypeConfig);

            await mAnimalRepository.UpdateAsync(animal);

            var animalResponse = GetAnimalResponse(animal);

            return animalResponse;
        }

        public void WashAnimal(Animal animal, AnimalTypeConfig animalTypeConfig)
        {
            int currentCleanlinessLevel = mAnimalStateStrategy.CalculateCleanlinessLevel(animal, animalTypeConfig);

            //Adjust the happiness level stored in the repository for the animal to take in to account that
            //it has been stroked
            animal.CleanlinessValueStored = currentCleanlinessLevel + animalTypeConfig.CleanlinessChangePerWash;

            //If the happiness level exceeds the maximum allowed then, just for now, clamp the value
            //at the maximum
            if (animal.CleanlinessValueStored > animalTypeConfig.CleanlinessMaxValue)
                animal.CleanlinessValueStored = animalTypeConfig.CleanlinessMaxValue;

            animal.LastTimeWashed = DateTime.Now;
        }

        public async Task<string> DeleteAnimal(string animalId)
        {
            Animal animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            //We don't allow animals to be deleted if they still have an owner. Currently
            //the animal needs to be removed from the owner (using the UsersController)
            //before deleting the animal
            //TODO: When we change to a microservices architecture, consider allowing an animal
            //to be deleted even if it still has an owner and use the Event Sourcing pattern to
            //publish an event AnimalDeleted to a message broker. The UserService would subscribe
            //to receive the event and update the user in its internal store to remove the animal
            //from the list of animals the user owns.
            if (animal.OwnerId != null)
            {
                throw new ArgumentException("Cannot delete animal because it still has an owner");
            }

            await mAnimalRepository.DeleteAsync(animal);

            return animal.Id;
        }

        ////////////////////////////////////////////////////////////////////////////////
        //TODO: When we change to a microservices architecture this method would handle the
        //AnimalRemovedFromOwner event that would be published by the UserService
        public async Task RemoveOwnerFromAnimal(string animalId, string ownerId)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            if (animal.OwnerId != ownerId)
                throw new ArgumentException("Animal is not owned by" + ownerId.ToString());

            animal.OwnerId = null;

            await mAnimalRepository.UpdateAsync(animal);
        }

        //TODO: When we change to a microservices architecture this method would handle the
        //AnimalAssociatedWithOwner event that would be published by the UserService
        public async Task AssociateOwnerWithAnimal(string animalId, string ownerId)
        {
            var animal = await mAnimalRepository.GetAnimalAsync(animalId);

            if (animal == null)
                throw new NotFoundException("Animal not found");

            if (animal.OwnerId != null)
                throw new ArgumentException("Animal already has an owner");

            animal.OwnerId = ownerId;

            await mAnimalRepository.UpdateAsync(animal);

        }
        ////////////////////////////////////////////////////////////////////////////////

        private List<AnimalResponse> GetAnimalResponseList(IEnumerable<Animal> animals)
        {
            var animalsResponse = new List<AnimalResponse>(animals.Count());
            foreach (var animal in animals)
            {
                AnimalResponse response = GetAnimalResponse(animal);

                if (response == null)
                    return null;

                animalsResponse.Add(response);
            }

            return animalsResponse;
        }

        private AnimalResponse GetAnimalResponse(Animal animal)
        {
            AnimalTypeConfig animalTypeConfig = mConfigService.GetAnimalTypeConfig(animal.TypeOfAnimal);

            if (animalTypeConfig == null)
                throw new Exception("No config file found for animal type");

            //These values are intentionally calculated dynamically, otherwise we would need a scheduled task
            //that continuously wrote these values to the database. The scheduled task would tick per the frequency
            //of the IAnimalStateChangeStrategy that is configured (e.g. currently we are using per minute but
            //this could just as easily, and more likely, be per second). Calculating dynamically means we only
            //write these values to the database when the owner interacts with the pet in some way, and then only
            //the value affected by the interaction is written (e.g. if feeding a pet then only the hunger level
            //needs to be written). Therefore calculating the values dynamically greatly reduces the writes to the
            //database. See FeedAnimal, StrokeAnimal and WashAnimal for writing each of these values to the database
            //when the user interacts with an animal
            float happinessLevel = mAnimalStateStrategy.CalculateHappinessLevelAsPercentage(animal, animalTypeConfig);
            float hungerLevel = mAnimalStateStrategy.CalculateHungerLevelAsPercentage(animal, animalTypeConfig);
            float cleanlinessLevel = mAnimalStateStrategy.CalculateCleanlinessLevelAsPercentage(animal, animalTypeConfig);
            
            return new AnimalResponse()
            {
                Id = animal.Id,
                OwnerId = animal.OwnerId,
                Name = animal.Name,
                TypeOfAnimal = animal.TypeOfAnimal,
                HappinessLevel = happinessLevel,
                HungerLevel = hungerLevel,
                CleanlinessLevel = cleanlinessLevel
            };
        }

    }
}
