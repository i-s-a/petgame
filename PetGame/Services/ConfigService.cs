﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetGame.Models;
using PetGame.Repositories;

namespace PetGame.Services
{
    public class ConfigService
    {
        private IConfigRepository mConfigRepository;

        public ConfigService(IConfigRepository configRepository)
        {
            mConfigRepository = configRepository;
        }

        public void AddAnimalTypeConfig(AnimalTypeConfig animalTypeConfig)
        {
            mConfigRepository.AddAnimalTypeConfig(animalTypeConfig);
        }

        public AnimalTypeConfig GetAnimalTypeConfig(AnimalType animalType)
        {
            return mConfigRepository.GetAnimalTypeConfig(animalType);
        }
    }
}
