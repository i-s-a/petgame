﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetGame.Scheduler
{
    public interface INotifier
    {
        Task Send(string userId, string message);
    }
}
