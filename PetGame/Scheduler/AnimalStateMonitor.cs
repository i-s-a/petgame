﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using PetGame.Services;

namespace PetGame.Scheduler
{
    public class AnimalStateMonitor : BackgroundService
    {
        private AnimalService mAnimalService = null;
        private ConfigService mConfigService = null;
        private INotificationFactory mNotificationFactory = null;

        public AnimalStateMonitor(INotificationFactory notificationFactory, AnimalService animalService, ConfigService configService)
        {
            mNotificationFactory = notificationFactory;
            mAnimalService = animalService;
            mConfigService = configService;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //TODO
            //We need to use the animal service to retrieve each animal that
            //is below the minimum treshold for hunger, happiness or cleanliness
            //and send to the owner of these animals a notification notifying them
            //that they need to be fed, stroked or washed. If more than one animal
            //with the same owner needs one or more interaction (e.g. washing and
            //feeding) then we only want to send 1 notification!! We use a
            //notification factory to retrieve the type of notification to use for
            //a specific user (e.g. mobile notification, email, etc.)

            //The animal service will need to be updated to retrieve the animals
            //that need attention so that the notifications can be sent to the
            //owners!!

            throw new NotImplementedException();
        }
    }
}
