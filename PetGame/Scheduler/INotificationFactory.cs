﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetGame.Scheduler
{
    public interface INotificationFactory
    {
        INotifier GetNotifier(string userId);
    }
}
