﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetGame.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;

namespace PetGame.Repositories
{
    public class CosmosDbUserRepository : IUserRepository
    {
        private const string DatabaseId = "PetGameUserDb";
        private const string DocumentCollectionId = "PetGameUser";
        private const string EndpointUri = "{EndpointUri}";
        private const string PrimaryKey = "{PrimaryKey}";
        private DocumentClient mClient = null;
        private IConfigRepository mConfigRepository = null;

        public CosmosDbUserRepository(IConfigRepository configRepository)
        {
            mConfigRepository = configRepository;
            mClient = new DocumentClient(new Uri(EndpointUri), PrimaryKey);
        }

        public ICache<string, Models.User> Cache { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void Add(Models.User user)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(Models.User user)
        {
            throw new NotImplementedException();
        }

        public void Delete(Models.User user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Models.User user)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Models.User> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Models.User>> GetAllUsersAsync()
        {
            throw new NotImplementedException();
        }

        public Models.User GetUser(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<Models.User> GetUserAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(Models.User user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUserAsync(Models.User user)
        {
            throw new NotImplementedException();
        }
    }
}
