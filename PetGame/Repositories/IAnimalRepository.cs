﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PetGame.Models;

namespace PetGame.Repositories
{
    public interface IAnimalRepository
    {
        Task AddAsync(Animal animal);
        Task DeleteAsync(Animal animal);
        Task<Animal> GetAnimalAsync(string animalId);
        Task<IEnumerable<Animal>> GetAllAnimalsAsync();
        Task<IEnumerable<Animal>> GetAllAnimalsByTypeAsync(AnimalType animalType);
        Task UpdateAsync(Animal animal);
    }
}
