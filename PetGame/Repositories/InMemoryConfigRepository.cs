﻿using PetGame.Models;

namespace PetGame.Repositories
{
    public class InMemoryConfigRepository : IConfigRepository
    {
        private ApiContext mContext;
        private ICache<AnimalType, AnimalTypeConfig> mAnimalTypeConfigCache = null;

        public InMemoryConfigRepository(ApiContext context)
        {
            mContext = context;
        }

        public void AddAnimalTypeConfig(AnimalTypeConfig animalTypeConfig)
        {
            mContext.AnimalConfig.Add(animalTypeConfig);
            mContext.SaveChanges();

            AddAnimalTypeConfigToCache(animalTypeConfig.TypeOfAnimal, animalTypeConfig);

        }

        public AnimalTypeConfig GetAnimalTypeConfig(AnimalType animalType)
        {
            var animalTypeConfig = GetAnimalTypeConfigFromCache(animalType);

            if (animalTypeConfig == null)
            {
                animalTypeConfig = mContext.AnimalConfig.Find(animalType);

                if (animalTypeConfig != null)
                {
                    AddAnimalTypeConfigToCache(animalType, animalTypeConfig);
                }
            }

            return animalTypeConfig;
        }

        private void AddAnimalTypeConfigToCache(AnimalType animalType, AnimalTypeConfig animalTypeConfig)
        {
            if (mAnimalTypeConfigCache != null)
            {
                mAnimalTypeConfigCache.Add(animalType, animalTypeConfig);
            }
        }

        private AnimalTypeConfig GetAnimalTypeConfigFromCache(AnimalType animalType)
        {
            AnimalTypeConfig animalTypeConfig = null;

            if (mAnimalTypeConfigCache != null)
            {
                animalTypeConfig = mAnimalTypeConfigCache.Get(animalType);
            }

            return animalTypeConfig;
        }
    }
}
