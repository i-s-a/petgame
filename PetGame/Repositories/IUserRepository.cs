﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PetGame.Models;

namespace PetGame.Repositories
{
    public interface IUserRepository
    {
        /// <summary>
        /// Add a new user to the repository
        /// </summary>
        /// <param name="request">Request containing the name of the user to be added</param>
        /// <returns>The new user just created</returns>
        void Add(User user);

        /// <summary>
        /// Add a new user to the repository asynchronously
        /// </summary>
        /// <param name="request">Request containing the name of the user to be added</param>
        /// <returns>The new user just created</returns>
        Task AddAsync(User user);

        /// <summary>
        /// Delete a user from the repository
        /// </summary>
        /// <param name="request">Request containing the ID of the user to be deleted</param>
        /// <returns>The user just deleted</returns>
        void Delete(User user);

        /// <summary>
        /// Delete a user from the repository asynchronously
        /// </summary>
        /// <param name="request">Request containing the ID of the user to be deleted</param>
        /// <returns>The user just deleted</returns>
        Task DeleteAsync(User user);

        /// <summary>
        /// Retrieve a user from the repository
        /// </summary>
        /// <param name="userId">The ID of the user to be retrieved</param>
        /// <returns>The user in the repository</returns>
        User GetUser(string userId);

        /// <summary>
        /// Retrieve a user from the repository asynchronously
        /// </summary>
        /// <param name="userId">The ID of the user to be retrieved</param>
        /// <returns>The User in the repository</returns>
        Task<User> GetUserAsync(string userId);

        /// <summary>
        /// Retrieve all users from the repository
        /// </summary>
        /// <returns>A list of all users</returns>
        IEnumerable<User> GetAllUsers();

        /// <summary>
        /// Retrieve all users from the repository asynchronously
        /// </summary>
        /// <returns>A list of all users</returns>
        Task<IEnumerable<User>> GetAllUsersAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        void UpdateUser(User user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task UpdateUserAsync(User user);
    }
}
