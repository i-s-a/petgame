﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetGame.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;

namespace PetGame.Repositories
{
    public class CosmosDbAnimalRepository : IAnimalRepository
    {
        private const string DatabaseId = "PetGameAnimalDb";
        private const string DocumentCollectionId = "PetGameAnimal";
        private const string EndpointUri = "{EndpointUri}";
        private const string PrimaryKey = "{PrimaryKey}";
        private DocumentClient mClient = null;
        private IConfigRepository mConfigRepository = null;

        public CosmosDbAnimalRepository(IConfigRepository configRepository)
        {
            mConfigRepository = configRepository;
            mClient = new DocumentClient(new Uri(EndpointUri), PrimaryKey);
        }

        public Task AddAsync(Animal animal)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Animal animal)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Animal>> GetAllAnimalsAsync()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Animal>> GetAllAnimalsByTypeAsync(AnimalType animalType)
        {
            throw new NotImplementedException();
        }

        public Task<Animal> GetAnimalAsync(string animalId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Animal animal)
        {
            throw new NotImplementedException();
        }
    }
}
