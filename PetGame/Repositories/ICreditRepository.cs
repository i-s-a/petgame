﻿using System.Threading.Tasks;
using PetGame.Models;

namespace PetGame.Repositories
{
    public interface ICreditRepository
    {
        Task AddAsync(Credit credit);
        Task DeleteAsync(Credit credit);
        Task<Credit> GetAsync(string userId);
        Task UpdateAsync(Credit credit);
    }
}
