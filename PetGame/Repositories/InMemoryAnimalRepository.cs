﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetGame.Models;

namespace PetGame.Repositories
{
    public class InMemoryAnimalRepository : IAnimalRepository
    {
        private ApiContext mContext;

        public InMemoryAnimalRepository(ApiContext context)
        {
            mContext = context;
        }

        public async Task AddAsync(Animal animal)
        {
            mContext.Animals.Add(animal);
            await mContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Animal animal)
        {
            mContext.Animals.Remove(animal);
            await mContext.SaveChangesAsync();            
        }

        public async Task<Animal> GetAnimalAsync(string animalId)
        {
            var animal = await mContext.Animals.SingleOrDefaultAsync(x => x.Id == animalId);
            return animal;
        }

        public async Task<IEnumerable<Animal>> GetAllAnimalsAsync()
        {
            var animals = await mContext.Animals.ToListAsync();

            return animals;
        }

        public async Task<IEnumerable<Animal>> GetAllAnimalsByTypeAsync(AnimalType animalType)
        {
            var animals = await mContext.Animals.Where(x => x.TypeOfAnimal == animalType).ToListAsync();

            return animals;
        }

        public async Task UpdateAsync(Animal animal)
        {
            mContext.Animals.Update(animal);
            await mContext.SaveChangesAsync();
        }
    }
}
