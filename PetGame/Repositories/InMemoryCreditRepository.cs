﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetGame.Models;

namespace PetGame.Repositories
{
    public class InMemoryCreditRepository : ICreditRepository
    {
        private ApiContext mContext;

        public InMemoryCreditRepository(ApiContext context)
        {
            mContext = context;
        }

        public async Task AddAsync(Credit credit)
        {
            mContext.Credits.Add(credit);
            await mContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Credit credit)
        {
            mContext.Credits.Remove(credit);
            await mContext.SaveChangesAsync();            
        }

        public async Task<Credit> GetAsync(string userId)
        {
            var credit = await mContext.Credits.SingleOrDefaultAsync(x => x.Id == userId);
            return credit;
        }

        public async Task UpdateAsync(Credit credit)
        {
            mContext.Credits.Update(credit);
            await mContext.SaveChangesAsync();
        }
    }
}
