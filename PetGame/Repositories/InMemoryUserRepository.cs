﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetGame.Models;

namespace PetGame.Repositories
{
    public class InMemoryUserRepository : IUserRepository
    {
        private ApiContext mContext;

        public InMemoryUserRepository(ApiContext context)
        {
            mContext = context;
        }
        
        public void Add(User user)
        {
            mContext.Users.Add(user);
            mContext.SaveChanges();
        }

        public async Task AddAsync(User user)
        {
            mContext.Users.Add(user);
            await mContext.SaveChangesAsync();
        }

        public void Delete(User user)
        {
            mContext.Users.Remove(user);
            mContext.SaveChanges();
        }

        public async Task DeleteAsync(User user)
        {
            mContext.Users.Remove(user);
            await mContext.SaveChangesAsync();
        }

        public User GetUser(string userId)
        {
            var user = mContext.Users
                .Include(x => x.AnimalIds)
                .SingleOrDefault(x => x.Id == userId);

            return user;
        }

        public async Task<User> GetUserAsync(string userId)
        {
            var user = await mContext.Users
                .Include(x => x.AnimalIds)
                .SingleOrDefaultAsync(x => x.Id == userId);

            return user;
        }

        public IEnumerable<User> GetAllUsers()
        {
            var users = mContext.Users
                .Include(x => x.AnimalIds)
                .ToList();

            return users;
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            var users = await mContext.Users
                .Include(x => x.AnimalIds)
                .ToListAsync();

            return users;
        }

        public void UpdateUser(User user)
        {
            mContext.Users.Update(user);
            mContext.SaveChanges();
        }

        public async Task UpdateUserAsync(User user)
        {
            mContext.Users.Update(user);
            await mContext.SaveChangesAsync();
        }
    }
}
