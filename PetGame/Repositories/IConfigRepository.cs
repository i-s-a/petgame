﻿using PetGame.Models;

namespace PetGame.Repositories
{
    public interface IConfigRepository
    {
        /// <summary>
        /// Add an animal type configuration to the repository that describes how happy and hungry an animal
        /// type becomes when an event occurs (e.g. a certain amount of time passes) or is fed or stroked
        /// </summary>
        /// <param name="animalTypeConfig">The config describing the animal type</param>
        void AddAnimalTypeConfig(AnimalTypeConfig animalTypeConfig);

        /// <summary>
        /// Retrieve an animal type configuration from the repository
        /// </summary>
        /// <param name="animalType"></param>
        /// <returns></returns>
        AnimalTypeConfig GetAnimalTypeConfig(AnimalType animalType);
    }
}
