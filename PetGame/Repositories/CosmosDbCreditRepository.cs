﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetGame.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;

namespace PetGame.Repositories
{
    public class CosmosDbCreditRepository : ICreditRepository
    {
        private const string DatabaseId = "PetGameCreditDb";
        private const string DocumentCollectionId = "PetGameCredit";
        private const string EndpointUri = "{EndpointUri}";
        private const string PrimaryKey = "{PrimaryKey}";
        private DocumentClient mClient = null;
        private IConfigRepository mConfigRepository = null;

        public CosmosDbCreditRepository(IConfigRepository configRepository)
        {
            mConfigRepository = configRepository;
            mClient = new DocumentClient(new Uri(EndpointUri), PrimaryKey);
        }

        public Task AddAsync(Credit credit)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Credit credit)
        {
            throw new NotImplementedException();
        }

        public Task<Credit> GetAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Credit credit)
        {
            throw new NotImplementedException();
        }
    }
}
