﻿using System.Collections.Generic;

namespace PetGameClient.Responses
{
    public class UserResponse
    {
        public string Id { set; get; }

        public string Name { set; get; }

        public List<string> AnimalIds { set; get; }
    }
}
