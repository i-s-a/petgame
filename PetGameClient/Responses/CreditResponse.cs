﻿namespace PetGameClient.Responses
{
    public class CreditResponse
    {
        public string Id { get; set; }
		public int Amount {get; set; }
    }
}
