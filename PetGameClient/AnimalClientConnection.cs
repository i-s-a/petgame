﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PetGameClient.Requests;
using PetGameClient.Responses;

namespace PetGameClient
{
    public class AnimalClientConnection : PetGameClientConnection
    {
        private readonly string ADD_ANIMAL_URL = "animals/create";
        private readonly string DELETE_ANIMAL_URL = "animals";
        private readonly string UPDATE_ANIMAL_URL = "animals";
        private readonly string GET_ANIMAL_URL = "animals";
        private readonly string STROKE_ANIMAL_URL = "animals/stroke";
        private readonly string FEED_ANIMAL_URL = "animals/feed";
        private readonly string WASH_ANIMAL_URL = "animals/wash";

        public AnimalClientConnection(string baseUri) : base(baseUri)
        {
        }

        public Task<AnimalResponse> AddAnimal(NewAnimalRequest request)
        {
            return DoPost<NewAnimalRequest, AnimalResponse>(ADD_ANIMAL_URL, request);
        }

        public Task<string> DeleteAnimal(string animalId)
        {
            return DoDelete<string>(animalId, DELETE_ANIMAL_URL);
        }

        public Task<AnimalResponse> UpdateAnimal(string animalId, UpdateAnimalRequest request)
        {
            return DoPut<UpdateAnimalRequest, AnimalResponse>(animalId, request, UPDATE_ANIMAL_URL);
        }

        public Task<AnimalResponse> GetAnimal(string animalId)
        {
            return DoGet<AnimalResponse>(animalId, GET_ANIMAL_URL);
        }

        public Task<List<AnimalResponse>> GetAllAnimals()
        {
            return DoGetAll<List<AnimalResponse>>(GET_ANIMAL_URL);
        }

        public Task<AnimalResponse> StrokeAnimal(StrokeAnimalRequest request)
        {
            return DoPost<StrokeAnimalRequest, AnimalResponse>(STROKE_ANIMAL_URL, request);
        }

        public Task<AnimalResponse> FeedAnimal(FeedAnimalRequest request)
        {
            return DoPost<FeedAnimalRequest, AnimalResponse>(FEED_ANIMAL_URL, request);
        }

        public Task<AnimalResponse> WashAnimal(WashAnimalRequest request)
        {
            return DoPost<WashAnimalRequest, AnimalResponse>(WASH_ANIMAL_URL, request);
        }
    }
}
