﻿using System;

namespace PetGameClient
{
    public class PetGameClientException : Exception
    {
        public PetGameClientException(string baseUri, string message) 
            : base(message)
        {
        }

        public PetGameClientException(string baseUri, string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public string BaseUri { get; set; }
    }
}
