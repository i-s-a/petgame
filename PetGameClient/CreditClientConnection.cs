﻿using System.Threading.Tasks;
using PetGameClient.Requests;
using PetGameClient.Responses;

namespace PetGameClient
{
    public class CreditClientConnection : PetGameClientConnection
    {
        private readonly string ADD_UPDATE_USER_CREDIT_URL = "credits";
        private readonly string DELETE_USER_CREDIT_URL = "credits";
        private readonly string GET_USER_CREDIT_URL = "credits";

        public CreditClientConnection(string baseUri) : base(baseUri)
        {
        }

        public Task<CreditResponse> GetUserCredit(string userId)
        {
            return DoGet<CreditResponse>(userId, GET_USER_CREDIT_URL);
        }

        public Task<CreditResponse> CreateOrUpdateUserCredit(string userId, UpdateUserCreditRequest request)
        {
            return DoPut<UpdateUserCreditRequest, CreditResponse>(userId, request, ADD_UPDATE_USER_CREDIT_URL);
        }

        public Task<string> DeleteUserCredit(string userId, bool force = false)
        {
            string url = string.Format($"{DELETE_USER_CREDIT_URL}");
            string query = string.Format($"?force={force}");

            return DoDelete<string>(userId, url, query);
        }
    }
}
