﻿using System;

namespace PetGameClient
{
    public enum AnimalType
    {
        Cat,
        Dog,
        Rabbit
    }

    public class AnimalHelper
    {
        public static AnimalType GetAnimalTypeFromString(string type)
        {
            if (type == "cat")
                return AnimalType.Cat;
            else if (type == "dog")
                return AnimalType.Dog;
            else if (type == "rabbit")
                return AnimalType.Rabbit;
            else
                throw new Exception("Invalid animal type");
        }
    }
}
