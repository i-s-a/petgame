﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PetGameClient.Requests;
using PetGameClient.Responses;

namespace PetGameClient
{
    public class UserClientConnection : PetGameClientConnection
    {
        private readonly string ADD_USER_URL = "users/create";
        private readonly string DELETE_USER_URL = "users";
        private readonly string UPDATE_USER_URL = "users";
        private readonly string GET_USER_URL = "users";

        public UserClientConnection(string baseUri) : base(baseUri)
        {
        }

        public Task<UserResponse> AddUser(NewUserRequest request)
        {
            return DoPost<NewUserRequest, UserResponse>(ADD_USER_URL, request);
        }

        public Task<string> DeleteUser(string userId)
        {
            return DoDelete<string>(userId, DELETE_USER_URL);
        }

        public Task<UserResponse> UpdateUser(string userId, UpdateUserRequest request)
        {
            return DoPut<UpdateUserRequest, UserResponse>(userId, request, UPDATE_USER_URL);
        }

        public Task<UserResponse> GetUser(string userId)
        {
            return DoGet<UserResponse>(userId, GET_USER_URL);
        }

        public Task<List<UserResponse>> GetAllUsers()
        {
            return DoGetAll<List<UserResponse>>(GET_USER_URL);
        }

        public Task<UserResponse> AssociateAnimalWithOwner(string userId, string animalId)
        {
            string uri = string.Format("{0}/{1}/animals", UPDATE_USER_URL, userId);
            return DoPut<string, UserResponse>(animalId, "[]", uri);
        }

        public Task<UserResponse> RemoveAnimalFromOwner(string userId, string animalId)
        {
            string uri = string.Format("{0}/{1}/animals", DELETE_USER_URL, userId);
            return DoDelete<UserResponse>(animalId, uri);
        }
    }
}
