﻿namespace PetGameClient.Requests
{
    public class UpdateUserCreditRequest
    {
        public int Amount { get; set; }
    }
}
