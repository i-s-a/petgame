﻿namespace PetGameClient.Requests
{
    public class StrokeAnimalRequest
    {
        public string UserId { get; set; }

        public string AnimalId { get; set; }
    }
}
