﻿namespace PetGameClient.Requests
{
    public class DeleteAnimalRequest
    {
        public string AnimalId { get; set; }
    }
}
