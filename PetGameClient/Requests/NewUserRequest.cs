﻿namespace PetGameClient.Requests
{
    public class NewUserRequest
    {
        public string Username { get; set; }
    }

}
