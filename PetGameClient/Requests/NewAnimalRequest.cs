﻿namespace PetGameClient.Requests
{
    public class NewAnimalRequest
    {
        public AnimalType TypeOfAnimal { get; set; }

        public string Name { get; set; }
    }
}
