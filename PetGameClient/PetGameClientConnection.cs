﻿using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace PetGameClient
{
    public class PetGameClientConnection
    {
        private HttpClient mHttpClient = null;

        public PetGameClientConnection(string baseUri)
        {
            Uri uri = new Uri(baseUri);

            mHttpClient = new HttpClient
            {
                BaseAddress = uri
            };

            mHttpClient.DefaultRequestHeaders.Accept.Clear();
            mHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        
        public async Task<HttpResponseMessage> PostAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
                await mHttpClient.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> PutAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
            await mHttpClient.PutAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        public async Task<Response> DoGet<Response>(string id, string url)
        {
            string uri = string.Format("{0}/{1}", url, id);
            var response = await mHttpClient.GetAsync(uri);
            var contentResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Response>(contentResponse);
            }
            else
            {
                throw new PetGameClientException(url,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), contentResponse));
            }
        }

        public async Task<Response> DoGetAll<Response>(string url)
        {
            var response = await mHttpClient.GetAsync(url);
            var contentResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Response>(contentResponse);
            }
            else
            {
                throw new PetGameClientException(url,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), contentResponse));
            }
        }

        public async Task<Response> DoPost<Request, Response>(string url, Request request)
        {
            var response = await PostAsJson<Request>(url, request);
            var readResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                var deserializedResponse = JsonConvert.DeserializeObject<Response>(readResponse);
                return deserializedResponse;
            }
            else
            {
                throw new PetGameClientException(url,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), readResponse));
            }
        }

        public async Task<Response> DoPut<Request, Response>(string id, Request request, string url)
        {
            string uri = string.Format("{0}/{1}", url, id);
            var response = await PutAsJson<Request>(uri, request);
            var putResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Response>(putResponse);
            }
            else
            {
                throw new PetGameClientException(url,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), putResponse));
            }
        }

        public async Task<Response> DoDelete<Response>(string id, string url, string queryString = "")
        {
            string uri = string.Format("{0}/{1}{2}", url, id, queryString);
            var response = await mHttpClient.DeleteAsync(uri);
            var deleteResponse = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Response>(deleteResponse);
            }
            else
            {
                throw new PetGameClientException(url,
                    string.Format("{0} - {1}", response.StatusCode.ToString(), deleteResponse));
            }
        }
    }
}
